import Vue from 'vue';
import Vuex from 'vuex';

import { alert } from './alert.module';
import { authentication } from './authentication.module';
import { users } from './users.module';

Vue.use(Vuex);

//_store Ordner enthält alle Vuex-Module und alles im Bezug zum Vuex Store
export const store = new Vuex.Store({
    modules: {
        alert,
        authentication,
        users,
    }
});