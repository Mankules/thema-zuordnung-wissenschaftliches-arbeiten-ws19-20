import  userService from '../_services/user.service';
import { router } from '../_helpers';
import { log } from "util";
const user = JSON.parse(localStorage.getItem('user'));
const initialState = user
    ? { status: { loggedIn: true }, user }
    : { status: {}, user: null };
//Ist der User im LocalStorage gespeichert? ->Ja-> loggedIn:true

export const authentication = {
    namespaced: true,
    state: initialState,
    actions: {
        login({ dispatch, commit }, { username, password }) {
            commit('loginRequest', { username });

            userService.login(username, password)
                .then(
                    user => {
                        commit('loginSuccess', user);

                        if (user.dataProtectionAccepted == false) {
                            router.push("/protection")
                        } else {
                                if (user.role == 'ADMIN') {
                                    router.push('/manageThemas')
                                } else {
                                    router.push('/themas')
                                }
                        }

                        log("Successfully logged in (auth.module): " + user.token)
                    },
                    error => {
                        commit('loginFailure', error);
                        dispatch('alert/error', "Der Benutzername oder das Password sind inkorrekt!", { root: true });
                    }
                );
        },
        logout({ commit }) {
            userService.logout();
            commit('logout');
        },
        register({ commit }, { username, name, surname, password }) {
            log("REGISTERING: " + username + name + surname + password)
            userService.register(username, name, surname, password);
            commit('register');
        }
    },
    mutations: {
        acceptDataProtection(state){
            state.user.dataProtectionAccepted = true;
            localStorage.setItem('user',JSON.stringify(state.user))
        },
        loginRequest(state, user) {
            state.status = { loggingIn: true };
            state.user = user;
        },
        loginSuccess(state, user) {
            log("LOGIN SUCCESS: " + user);
            state.status = { loggedIn: true };
            state.user = user;
        },
        loginFailure(state) {
            state.status = {};
            state.user = null;
        },
        logout(state) {
            state.status = {};
            state.user = null;
        },
        register(state) {
            state.status = {};
            state.user = null;
        }
    }
}