import axios from "axios";
import  {mainURL}  from '../constants/URL.js'
import toast from "../_helpers/toast.js";

class ThemaService {
    autoAssignThema() {
        return axios.post(mainURL+'/themas/autoAssignThema').then(response => toast.success(response.data));
    }

    getAll() {
        return axios.get(mainURL+'/themas');
    }
    sendPrios(prios) {
        return axios.post(mainURL+'/themas/sendPrios', {
            prios:prios
        }).then(response => toast.success(response.data));
    }
    getAllRawThemas(){
        return axios.get(mainURL+'/rawThemas');
    }
    getAllThemaDataForUser(userId) {
        return axios.get(mainURL+'/assignThema/'+userId);
    }
    assignThema(themaId,userId) {
        return axios.post(mainURL+'/themas/'+themaId+'/assignThema/'+userId).then(response => toast.success(response.data));
    }
    deleteAssignThema(themaId,userId) {
        return axios.post(mainURL+'/themas/'+themaId+'/deleteAssignThema/'+userId).then(response => toast.success(response.data));
    }
    deleteThema(themaId) {
        return axios.delete(mainURL+'/thema/'+themaId+'/delete').then(response => toast.success(response.data));
    }
    excludeThema(themaId,userId) {
        return axios.post(mainURL+'/themas/'+themaId+'/excludeThema/'+userId).then(response => toast.success(response.data));
    }
    allowThema(themaId,userId) {
        return axios.post(mainURL+'/themas/'+themaId+'/allowThema/'+userId).then(response => toast.success(response.data));
    }
    createThema(name,description) {
        return axios.post(mainURL+'/createThema', {
            name :name,
            description : description
        }).then(response => toast.success(response.data));
    }
    getThemaDataDTO(themaId){
        return axios.get(mainURL+'/thema/'+themaId);
    }
    editThema(themaId,thema) {
        return axios.post(mainURL+'/thema/'+themaId+'/editThema', {
            name: thema.name,
            description: thema.description
        }).then(response => toast.success(response.data));
    }
}

const themaService = new ThemaService();
export default themaService;