import Vue from 'vue'
import App from './App.vue'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import { router } from './_helpers';
import { store } from './_store';
import {interceptors} from './_helpers';
import { FormRadioPlugin } from 'bootstrap-vue'
Vue.use(FormRadioPlugin)
import toast from './_helpers/toast.js'
//require('./assets/style.css')
require('./assets/material.min.css')

interceptors();

window.onunhandledrejection = function(event) {

  
  //handle error here
   //event.promise contains the promise object
   //event.reason contains the reason for the rejection
   if(event){
     
      
      //this.console.trace();
      let error = event.reason;
     toast.apiError(error);


      //Support for different Browers
      if(event.preventDefault){ event.preventDefault()}
      else if(event.stop) {
        event.stop()
      }
  
      event.returnValue = false;
      if(event.stopPropagation) event.stopPropagation(); 
     }
  
 }


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')


