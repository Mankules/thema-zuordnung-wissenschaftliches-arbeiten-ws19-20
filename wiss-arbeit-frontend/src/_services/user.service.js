/* eslint-disable no-unused-vars */
import { authHeader } from '../_helpers';
import axios from 'axios';
import { log } from "util";
import toast from '../_helpers/toast.js'
import  {mainURL}  from '../constants/URL.js'

class UserService {
    register(username,name,surname,password){
        return axios.post(mainURL+'/register', {
            username:username,
            name: name,
            surname: surname,
            passwordHash:password
        }).then(response => toast.success(response.data))
    }

    login(username, password) {

        const authUser = axios.post(mainURL+`/login`, {
            username: username,
            password: password

        })
            .then(user => {
                // login successful if there's a jwt token in the response
                if (user.data.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('user', JSON.stringify(user.data));
                }
                return user.data;
            })
        return authUser;
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('user');
    }

    getDTO() {
        return axios.get(mainURL+ '/users');
    }

    getPrivacyPolicy(){
        return axios.get(mainURL+'/privacy');
    }

    editPrivacyPolicy(text){
        return axios.post(mainURL+'/privacy/edit',{ 'text':text })
    }

    deleteUser(userId){
        return axios.delete(mainURL+ '/users/' + userId + '/delete').then(response => toast.success(response.data));
    }

    getCurrentUserObject(){
        return axios.get(mainURL+`/users/get`, {
            headers: authHeader()
        }).then(response => {
            return response.data
        })
    }

    changeDataProtectionStatus() {
        return axios.patch(mainURL+`/users/protection`, {dataProtectionAccepted: true})
    }

}

const userService = new UserService();
export default userService;