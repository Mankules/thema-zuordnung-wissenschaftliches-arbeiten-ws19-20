import axios from 'axios';
import { authHeader2 } from './auth-header';
export function interceptors() {
    
    axios.interceptors.request.use(function(config) {
        const token = authHeader2();
        if(token) {
            config.headers['Authorization'] = token;
            //log("Interceptors token: " + authHeader2())
        }
        return config;
    }, function(err) {
        return Promise.reject(err);
    })
   
}