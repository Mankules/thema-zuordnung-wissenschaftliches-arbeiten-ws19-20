import 'izitoast/dist/css/iziToast.min.css'
import iZtoast from 'izitoast'
import { log } from 'util';
const toast = {
    error: (message, title = 'Fehler') => {
        return iZtoast.error({
            title: title,
            message: message,
            position: 'center',
            timeout: false,
        });
    },
    success: (message, title = 'Success') => {
        return iZtoast.success({
            title: title,
            message: message,
            position: 'bottomLeft'
        });
    },
    apiError: (errorO, title = 'Fehler') => {

        if (errorO.response) {
         
          if(errorO.response.data.message) {
             log("" + errorO.response.data.message);
             toast.error(errorO.response.data.message,title);
          } else {
            toast.error("Der Server hat mit einem Fehler geantwortet!",title);
          }      
        } else if (errorO.request) {
            toast.error("Der Server hat nicht geantwortet!<br/>Bitte teilen sie dies ihren Lehrenden mit.",title);
        } else {
          log("" + errorO.message);
          toast.error( errorO.message);
        }        
    }
};

export default toast;