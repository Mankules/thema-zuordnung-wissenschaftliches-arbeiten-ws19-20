import Vue from 'vue'
import Router from 'vue-router'
import {routes}  from './routes.js'

Vue.use(Router)

export const router= new Router({
    mode: 'history',
    //base: process.env.BASE_URL,
    routes,
  })

  router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const publicPages = ['/login','/register'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('user');
    const user = JSON.parse(localStorage.getItem('user'));
  
    if(to.meta.title) document.title = to.meta.title;
    else document.title = "Themenvergabe";
    //log("Logged in status (Router): " + loggedIn + "AuthRequired Status: " + authRequired)
    //log("USER IN ROUTER BEFORE EACH: " + user)
    if (authRequired && !loggedIn) {
      return next('/login');
    }
   
  
    if(user){
      if(user.role == 'ADMIN'){
        return next()
      }
      if(user.dataProtectionAccepted==true){
        if(!to.meta.roles){
          return next()
        }
        if(to.meta.roles.includes(user.role)){
          next()
        } else {
          next(from)
        }
    } else if(to.path !=='/protection'){
        if(to.path !== '/login'){
          return next('/protection')
    }
    }
  }
    next();
  })
  