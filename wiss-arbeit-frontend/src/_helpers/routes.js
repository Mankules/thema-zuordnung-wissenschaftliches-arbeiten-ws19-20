import ThemaPage from '../components/ThemaPage.vue'
import ManageThemaPage from '../components/ManageThemaPage.vue'
import AdminAssignThemaPage from '../components/AdminAssignThemaPage.vue'
import EditThemaPage from '../components/EditThemaPage.vue'
import CreateThemaPage from '../components/CreateThemaPage.vue'

import LoginPage from '../auth_components/LoginPage.vue'
import RegisterPage from '../auth_components/RegisterPage.vue'
import HomePage from '../auth_components/HomePage.vue'
import DataProtection from '../auth_components/Data-Protection.vue'
import RoleTest from '../components/RoleAccessTest.vue'

export const routes = 
    [
        { path: '/themas', name: 'Themas', component: ThemaPage, meta: {roles:['STUDENT'],title:'Themen'}},
        { path: '/manageThemas', name: 'ManageThemas', component: ManageThemaPage, meta: {roles:['ADMIN'],title:'Benutzerverwaltung'}},
        { path: '/:userID/assignThema', name: 'AdminAssignThemaPage', component: AdminAssignThemaPage, meta: {roles:['ADMIN'],title:'Themazuweisung'}},
        { path: '/:themaId/editThema', name: 'EditThemaPage', component: EditThemaPage, meta: {roles:['ADMIN'],title:'Themabearbeitung'}},
        { path: '/createThema', name: 'CreateThemaPage', component: CreateThemaPage, meta: {roles:['ADMIN'],title:'Themaerstellung'}},

        
        //auth
        { path: '/login', component: LoginPage, meta: {title:'Login'} },
        { path: '/register', component: RegisterPage, meta: {title:'Registrierung'} },
             //auth TEST
            { path: '/', component: HomePage},
   
             // otherwise redirect to home
        { path: '*', redirect: '/' },
 
        { path: '/roletest', component: RoleTest},
        { path: '/protection', component: DataProtection, meta: {title:'Datenschutz'} }
    ]
