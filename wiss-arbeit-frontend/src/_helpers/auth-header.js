//Helfer-Funktion

export function authHeader(){
    //Gibt den den Authorisationsheader mit dem JWT-Token zurück
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.token) {
        return { 'Authorization': user.token};
    } else {
        return {};
    }
}

export function authHeader2(){
    //Gibt den den Authorisationsheader mit dem JWT-Token zurück
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.token) {
        return user.token;
    } else {
        return {};
    }
}