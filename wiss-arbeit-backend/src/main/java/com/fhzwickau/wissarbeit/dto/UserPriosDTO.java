package com.fhzwickau.wissarbeit.dto;

import java.util.ArrayList;
import java.util.List;

public class UserPriosDTO {

	private List<UserPriosDataDTO> prios = new ArrayList<UserPriosDataDTO>();

	public UserPriosDTO() {
		super();
	}

	public UserPriosDTO(List<UserPriosDataDTO> prios) {
		super();
		this.prios = prios;
	}

	public List<UserPriosDataDTO> getPrios() {
		return prios;
	}

	public void setPrios(List<UserPriosDataDTO> prios) {
		this.prios = prios;
	}
}
