package com.fhzwickau.wissarbeit.dto;

public class ThemaAdminDataDTO {

	private Long themaId;
	private String name;

	public ThemaAdminDataDTO(Long themaId, String name) {
		super();
		this.themaId = themaId;
		this.name = name;
	}

	public Long getThemaId() {
		return themaId;
	}

	public void setThemaId(Long themaId) {
		this.themaId = themaId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
