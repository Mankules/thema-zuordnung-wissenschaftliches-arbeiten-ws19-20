package com.fhzwickau.wissarbeit.dto;

public class AdminDataThemaDTO {

	private Long themaId;
	private String name;
	private String description;

	public AdminDataThemaDTO(Long themaId, String name, String description) {
		super();
		this.themaId = themaId;
		this.name = name;
		this.description = description;
	}

	public Long getThemaId() {
		return themaId;
	}

	public void setThemaId(Long themaId) {
		this.themaId = themaId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
