package com.fhzwickau.wissarbeit.dto;

import com.fhzwickau.wissarbeit.user.domain.Role;
import com.fhzwickau.wissarbeit.user.domain.User;

public class AuthUserDTO {
	private Long id;

	private String name;

	private String surname;

	private Role role;

	private String nickname;

	private String token;

	private Boolean dataProtectionAccepted;

	public AuthUserDTO() {

	}

	public AuthUserDTO(User user, String token) {
		name = user.getName();
		surname = user.getSurname();
		nickname = user.getNickname();
		role = user.getRole();
		this.token = token;
		this.dataProtectionAccepted = user.getDataProtectionAccepted();
	}

	/**
	 * @param id
	 * @param name
	 * @param surname
	 * @param role
	 * @param nickname
	 * @param token
	 * @param dataProtectionAccepted
	 */
	public AuthUserDTO(Long id, String name, String surname, Role role, String nickname, String token,
			Boolean dataProtectionAccepted) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.role = role;
		this.nickname = nickname;
		this.token = token;
		this.dataProtectionAccepted = dataProtectionAccepted;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Boolean getDataProtectionAccepted() {
		return dataProtectionAccepted;
	}

	public void setDataProtectionAccepted(Boolean dataProtectionAccepted) {
		this.dataProtectionAccepted = dataProtectionAccepted;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
