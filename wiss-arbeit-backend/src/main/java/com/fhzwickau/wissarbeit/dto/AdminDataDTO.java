package com.fhzwickau.wissarbeit.dto;

import java.util.ArrayList;
import java.util.List;

public class AdminDataDTO {

	private List<AdminDataThemaDTO> themas = new ArrayList<AdminDataThemaDTO>();
	private List<AdminDataUserDTO> users = new ArrayList<AdminDataUserDTO>();

	public AdminDataDTO(List<AdminDataThemaDTO> themas, List<AdminDataUserDTO> users) {
		super();
		this.themas = themas;
		this.users = users;
	}

	public List<AdminDataThemaDTO> getThemas() {
		return themas;
	}

	public void setThemas(List<AdminDataThemaDTO> themas) {
		this.themas = themas;
	}

	public List<AdminDataUserDTO> getUsers() {
		return users;
	}

	public void setUsers(List<AdminDataUserDTO> users) {
		this.users = users;
	}
}
