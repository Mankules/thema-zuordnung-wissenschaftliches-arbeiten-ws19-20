package com.fhzwickau.wissarbeit.dto;

import java.util.ArrayList;
import java.util.List;

public class ThemaAdminDTO {

	public List<ThemaAdminDataDTO> allowThemas = new ArrayList<ThemaAdminDataDTO>();
	public List<ThemaAdminDataDTO> excludeThemas = new ArrayList<ThemaAdminDataDTO>();
	public ThemaAdminDataDTO assignThema = null;

	public ThemaAdminDTO() {
		super();
	}

	public ThemaAdminDTO(List<ThemaAdminDataDTO> allowThemas, List<ThemaAdminDataDTO> excludeThemas,
			ThemaAdminDataDTO assignThema) {
		super();
		this.allowThemas = allowThemas;
		this.excludeThemas = excludeThemas;
		this.assignThema = assignThema;
	}

	public List<ThemaAdminDataDTO> getAllowThemas() {
		return allowThemas;
	}

	public void setAllowThemas(List<ThemaAdminDataDTO> allowThemas) {
		this.allowThemas = allowThemas;
	}

	public List<ThemaAdminDataDTO> getExcludeThemas() {
		return excludeThemas;
	}

	public void setExcludeThemas(List<ThemaAdminDataDTO> excludeThemas) {
		this.excludeThemas = excludeThemas;
	}

	public ThemaAdminDataDTO getAssignThema() {
		return assignThema;
	}

	public void setAssignThema(ThemaAdminDataDTO assignThema) {
		this.assignThema = assignThema;
	}

}
