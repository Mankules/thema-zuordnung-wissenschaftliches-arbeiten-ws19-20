package com.fhzwickau.wissarbeit.dto;

public class ThemaDataDTO {

	private String name;
	private String description;

	public ThemaDataDTO() {

	}

	public ThemaDataDTO(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
