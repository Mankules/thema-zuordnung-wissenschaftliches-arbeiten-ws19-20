package com.fhzwickau.wissarbeit.dto;

public class UserPriosDataDTO {

	private String themaId;
	private String prio;

	public UserPriosDataDTO() {
		super();
	}

	public UserPriosDataDTO(String themaId, String prio) {
		super();
		this.themaId = themaId;
		this.prio = prio;
	}

	public String getThemaId() {
		return themaId;
	}

	public void setThemaId(String themaId) {
		this.themaId = themaId;
	}

	public String getPrio() {
		return prio;
	}

	public void setPrio(String prio) {
		this.prio = prio;
	}
}
