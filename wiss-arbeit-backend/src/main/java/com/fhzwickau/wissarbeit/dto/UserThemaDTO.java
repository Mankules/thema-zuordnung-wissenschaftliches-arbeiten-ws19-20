package com.fhzwickau.wissarbeit.dto;

import java.util.ArrayList;
import java.util.List;

public class UserThemaDTO {

	private ThemaDataDTO assignThema;
	private List<UserThemaDataDTO> themas = new ArrayList<UserThemaDataDTO>();

	public UserThemaDTO(ThemaDataDTO assignThema, List<UserThemaDataDTO> themas) {
		super();
		this.assignThema = assignThema;
		this.themas = themas;
	}

	public ThemaDataDTO getAssignThema() {
		return assignThema;
	}

	public void setAssignThema(ThemaDataDTO assignThema) {
		this.assignThema = assignThema;
	}

	public List<UserThemaDataDTO> getThemas() {
		return themas;
	}

	public void setThemas(List<UserThemaDataDTO> themas) {
		this.themas = themas;
	}
}
