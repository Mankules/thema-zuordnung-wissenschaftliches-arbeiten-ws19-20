package com.fhzwickau.wissarbeit.dto;

public class AdminDataUserDTO {

	private Long userId;
	private String name;
	private String surname;
	private String username;
	private boolean dataProtectionAccepted;
	private String themaname;

	public AdminDataUserDTO(Long userId, String name, String surname, String username, boolean dataProtectionAccepted,
			String themaname) {
		super();
		this.userId = userId;
		this.name = name;
		this.surname = surname;
		this.username = username;
		this.dataProtectionAccepted = dataProtectionAccepted;
		this.themaname = themaname;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isDataProtectionAccepted() {
		return dataProtectionAccepted;
	}

	public void setDataProtectionAccepted(boolean dataProtectionAccepted) {
		this.dataProtectionAccepted = dataProtectionAccepted;
	}

	public String getThemaname() {
		return themaname;
	}

	public void setThemaname(String themaname) {
		this.themaname = themaname;
	}

}
