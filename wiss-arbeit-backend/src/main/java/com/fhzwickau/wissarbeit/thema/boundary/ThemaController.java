package com.fhzwickau.wissarbeit.thema.boundary;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fhzwickau.wissarbeit.config.ApiError;
import com.fhzwickau.wissarbeit.config.GeneralErrorException;
import com.fhzwickau.wissarbeit.dto.CreateThemaDTO;
import com.fhzwickau.wissarbeit.dto.ThemaDataDTO;
import com.fhzwickau.wissarbeit.dto.UserPriosDTO;
import com.fhzwickau.wissarbeit.security.service.CurrentUserUtil;
import com.fhzwickau.wissarbeit.thema.service.ThemaService;
import com.fhzwickau.wissarbeit.user.boundary.UserController;
import com.fhzwickau.wissarbeit.user.domain.Role;
import com.fhzwickau.wissarbeit.user.domain.User;
import com.fhzwickau.wissarbeit.user.service.UserService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ThemaController {

	Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	ThemaService themaService;
	@Autowired
	UserService userService;

	public ThemaController() {

	}

	/**
	 * Checks if the current User is a Admin
	 * 
	 * @return true/false is User Role Admin
	 */
	private boolean isUserAdmin() {
		Optional<User> user = CurrentUserUtil.getCurrentUser(userService);
		if (user.isPresent()) {
			if (user.get().getRole().equals(Role.ADMIN)) {
				return true;
			}
			return false;
		}
		return false;
	}

	/**
	 * This method returns a JSON with a DTO of all the visible thema for the user.
	 * 
	 * @return UserThemaDTO
	 */
	@GetMapping(value = "/themas", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllThemas() throws GeneralErrorException {
		Optional<User> user = CurrentUserUtil.getCurrentUser(userService);
		if (user.isPresent()) {
			return ResponseEntity.status(HttpStatus.OK).body(themaService.getAllThemas(user));
		}
		throw new GeneralErrorException(new ApiError(HttpStatus.BAD_REQUEST, "Der User konnte nicht gefunden wurden"));
	}

	/**
	 * This method takes the given JSON and calls the ThemaService to save the
	 * priority's of the user.
	 * 
	 * @return Status
	 */
	@PostMapping(value = "/themas/sendPrios", consumes = "application/json")
	public ResponseEntity<?> sendPrios(@RequestBody UserPriosDTO prios) throws GeneralErrorException {
		Optional<User> user = CurrentUserUtil.getCurrentUser(userService);
		if (user.isPresent()) {
			if (themaService.savePrios(prios, user)) {
				logger.info("Es wurden erfolgreich die Priorit�ten von " + user.get().getName() + " "
						+ user.get().getSurname() + " gespeichert.");
				return ResponseEntity.status(HttpStatus.OK).body("Die Abgabe der Priorit�ten war erfolgreich");
			}
			throw new GeneralErrorException(
					new ApiError(HttpStatus.BAD_REQUEST, "Die Abgabe der Priorit�ten war nicht erfolgreich"));

		}
		throw new GeneralErrorException(new ApiError(HttpStatus.BAD_REQUEST, "Der User konnte nicht gefunden wurden"));
	}

	/**
	 * This method returns a JSON with all the specific thema data for the given
	 * user.
	 * 
	 * @param userId, to get the specific thema data of the user
	 * 
	 * @return Status
	 */
	@GetMapping(value = "/assignThema/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllThemaDataForUser(@PathVariable String userId) throws GeneralErrorException {
		if (isUserAdmin()) {
			if (userService.getUserByID(Long.valueOf(userId)).isPresent()) {
				return ResponseEntity.status(HttpStatus.OK).body(themaService.getAllThemaDataForUser(userId));
			}
			throw new GeneralErrorException(
					new ApiError(HttpStatus.BAD_REQUEST, "Der User konnte nicht gefunden wurden"));
		}
		throw new GeneralErrorException(
				new ApiError(HttpStatus.BAD_REQUEST, "Der User hat keine Rechte zum anzeigen aller Themanamen"));
	}

	/**
	 * This method returns a JSON with all thema data for the admin.
	 * 
	 * @param themaID, to get the thema data
	 * 
	 * @return Status
	 */
	@GetMapping(value = "/thema/{themaId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getThemaDataDTO(@PathVariable String themaId) throws GeneralErrorException {
		if (isUserAdmin()) {
			if (themaService.getThema(themaId).isPresent()) {
				return ResponseEntity.status(HttpStatus.OK).body(themaService.getThemaDataDTO(themaId));
			}
			throw new GeneralErrorException(
					new ApiError(HttpStatus.BAD_REQUEST, "Das Thema konnte nicht gefunden wurden"));
		}
		throw new GeneralErrorException(
				new ApiError(HttpStatus.BAD_REQUEST, "Der User hat keine Rechte zum anzeigen des Themas"));
	}

	/**
	 * This method calls the ThemaService to assign the given thema to the given
	 * user
	 * 
	 * @param themaId, to get the thema
	 * @param userId,  to get the user
	 * 
	 * @return Status
	 */
	@PostMapping(value = "/themas/{themaId}/assignThema/{userId}")
	public ResponseEntity<?> assignThema(@PathVariable String themaId, @PathVariable String userId)
			throws GeneralErrorException {
		if (isUserAdmin()) {
			if (themaService.assignThema(themaId, userId)) {
				User user = userService.getUserByID(Long.valueOf(userId)).get();
				logger.info("Das Thema " + themaService.getThema(themaId).get().getName() + " wurde erfolgreich "
						+ user.getName() + " " + user.getSurname() + " zugewiesen.");
				return ResponseEntity.status(HttpStatus.OK).body("Dem User wurde erfolgreich das Thema Zugeordnet");
			}
			throw new GeneralErrorException(new ApiError(HttpStatus.BAD_REQUEST, "User oder Thema nicht gefunden"));
		}
		throw new GeneralErrorException(
				new ApiError(HttpStatus.BAD_REQUEST, "Der User hat keine Rechte zum Zuweisen von Thema einem User"));
	}

	/**
	 * This method calls the ThemaService to remove the assign of the given thema to
	 * the given user
	 * 
	 * @param themaId, to allow the thema to the user
	 * @param userId,  to get the user
	 * 
	 * @return Status
	 */
	@PostMapping(value = "/themas/{themaId}/deleteAssignThema/{userId}")
	public ResponseEntity<?> deleteAssignThema(@PathVariable String themaId, @PathVariable String userId)
			throws GeneralErrorException {
		if (isUserAdmin()) {
			if (themaService.deleteAssignThema(themaId, userId)) {
				User user = userService.getUserByID(Long.valueOf(userId)).get();
				logger.info("Bei dem Thema " + themaService.getThema(themaId).get().getName() + " von " + user.getName()
						+ " " + user.getSurname() + " wurde erfolgreich die Zuweisung gel�scht.");
				return ResponseEntity.status(HttpStatus.OK)
						.body("Dem User wurde erfolgreich die Thema Zuordnung gel�scht");
			}
			throw new GeneralErrorException(new ApiError(HttpStatus.BAD_REQUEST, "User oder Thema nicht gefunden"));
		}
		throw new GeneralErrorException(new ApiError(HttpStatus.BAD_REQUEST,
				"Der User hat keine Rechte zum L�schen der Zuweisung des Thema einem User"));
	}

	/**
	 * This method calls the ThemaService to add the given thema to the exclude
	 * thema list of the given user.
	 * 
	 * @param themaId, to get the thema
	 * @param userId,  to get the user
	 * 
	 * @return Status
	 */
	@PostMapping(value = "/themas/{themaId}/excludeThema/{userId}")
	public ResponseEntity<?> excludeThema(@PathVariable String themaId, @PathVariable String userId)
			throws GeneralErrorException {
		if (isUserAdmin()) {
			if (themaService.excludeThema(themaId, userId)) {
				return ResponseEntity.status(HttpStatus.OK).body("Dem User wurde erfolgreich das Thema ausgeschlossen");
			}
			throw new GeneralErrorException(new ApiError(HttpStatus.BAD_REQUEST, "User oder Thema nicht gefunden"));
		}
		throw new GeneralErrorException(new ApiError(HttpStatus.BAD_REQUEST,
				"Der User hat keine Rechte zum Ausschlie�en von Themen eines User"));
	}

	/**
	 * This method calls the ThemaService to remove the given thema from the exclude
	 * thema list of the given user.
	 * 
	 * @param themaId, to get the thema
	 * @param userId,  to get the user
	 * 
	 * @return Status
	 */
	@PostMapping(value = "/themas/{themaId}/allowThema/{userId}")
	public ResponseEntity<?> allowThema(@PathVariable String themaId, @PathVariable String userId)
			throws GeneralErrorException {
		if (isUserAdmin()) {
			if (themaService.allowThema(themaId, userId)) {
				return ResponseEntity.status(HttpStatus.OK).body("Dem User wurde erfolgreich das Thema erlaubt");
			}
			throw new GeneralErrorException(new ApiError(HttpStatus.BAD_REQUEST, "User oder Thema nicht gefunden"));
		}
		throw new GeneralErrorException(
				new ApiError(HttpStatus.BAD_REQUEST, "Der User hat keine Rechte zum Erlauben von Thema einem User"));
	}

	/**
	 * This method takes the given JSON checks it and calls the ThemaService to
	 * create the new thema.
	 * 
	 * @return Status
	 */
	@PostMapping(value = "/createThema", consumes = "application/json")
	public ResponseEntity<?> createThema(@RequestBody CreateThemaDTO thema) throws GeneralErrorException {
		if (isUserAdmin()) {
			if (thema.getName().isEmpty() || thema.getDescription().isEmpty()) {
				throw new GeneralErrorException(new ApiError(HttpStatus.BAD_REQUEST,
						"Der Name oder Beschreibung des Thema darf nicht leer sein"));
			}
			if (themaService.createThema(thema)) {
				logger.info("Das Thema " + thema.getName() + " wurde erfolgreich erstellt.");
				return ResponseEntity.status(HttpStatus.OK).body("Das Thema wurde erfolgreich erstellt");
			}
			throw new GeneralErrorException(
					new ApiError(HttpStatus.BAD_REQUEST, "Unbekannter Fehler beim Erstellen von dem Thema"));
		}
		throw new GeneralErrorException(
				new ApiError(HttpStatus.BAD_REQUEST, "Der User hat keine Rechte zum Erstellen von einem Thema"));
	}

	/**
	 * This method takes the given JSON and calls the ThemaService to edit the given
	 * thema with the new description and name.
	 * 
	 * @param themaId, to get the old thema
	 * 
	 * @return Status
	 */
	@PostMapping(value = "/thema/{themaId}/editThema", consumes = "application/json")
	public ResponseEntity<?> editThema(@RequestBody ThemaDataDTO thema, @PathVariable String themaId)
			throws GeneralErrorException {
		if (isUserAdmin()) {
			if (thema.getName().isEmpty() || thema.getDescription().isEmpty() || themaId.isEmpty()) {
				throw new GeneralErrorException(new ApiError(HttpStatus.BAD_REQUEST,
						"Der Name oder Beschreibung des Thema das bearbeitet werden soll darf nicht leer sein"));
			}
			if (themaService.editThema(themaId, thema)) {
				logger.info("Das Thema " + thema.getName() + " wurde erfolgreich bearbeitet.");
				return ResponseEntity.status(HttpStatus.OK).body("Das Thema wurde erfolgreich bearbeitet");
			}
			throw new GeneralErrorException(
					new ApiError(HttpStatus.BAD_REQUEST, "Unbekannter Fehler beim Bearbeiten von dem Thema"));
		}
		throw new GeneralErrorException(
				new ApiError(HttpStatus.BAD_REQUEST, "Der User hat keine Rechte zum Bearbeiten von einem Thema"));
	}

	/**
	 * This method calls the ThemaService to delete the given thema.
	 * 
	 * @param themaId, to get the thema
	 * 
	 * @return Status
	 */
	@DeleteMapping(value = "/thema/{themaId}/delete")
	public ResponseEntity<?> deleteThema(@PathVariable String themaId) throws GeneralErrorException {
		if (isUserAdmin()) {
			if (themaService.deleteThema(themaId)) {
				logger.info("Der Admin hat ein Thema gel�scht.");
				return ResponseEntity.status(HttpStatus.OK).body("Das Thema wurde erfolgreich gel�scht");
			}
			throw new GeneralErrorException(new ApiError(HttpStatus.BAD_REQUEST,
					"Das Thema konnte nicht gel�scht werden da es nicht gefunden wurde"));
		}
		throw new GeneralErrorException(
				new ApiError(HttpStatus.BAD_REQUEST, "Der User hat keine Rechte zum l�schen von einem Thema"));
	}

	/**
	 * This method calls the ThemaService to let the HungarianAlgorithm automatic
	 * assign all the remaining themas und users.
	 * 
	 * @return Status
	 */
	@PostMapping(value = "/themas/autoAssignThema")
	public ResponseEntity<?> allowThema() throws GeneralErrorException {
		if (isUserAdmin()) {
			if (themaService.autoAssignThema()) {
				return ResponseEntity.status(HttpStatus.OK).body("Den Usern wurde die Themen zugeordnet");
			}
			throw new GeneralErrorException(
					new ApiError(HttpStatus.BAD_REQUEST, "Fehler beim automatischen Zuweisen von Themen"));
		}
		throw new GeneralErrorException(new ApiError(HttpStatus.BAD_REQUEST,
				"Der User hat keine Rechte zum Ausf�hren des automatischen Zuweisen von Themen"));
	}
}
