package com.fhzwickau.wissarbeit.thema.domain;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ThemaRepository extends JpaRepository<Thema, Long> {

	Optional<Thema> findByName(String themaName);

}
