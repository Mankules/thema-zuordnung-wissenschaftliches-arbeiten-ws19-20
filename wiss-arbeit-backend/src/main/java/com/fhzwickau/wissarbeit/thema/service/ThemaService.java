package com.fhzwickau.wissarbeit.thema.service;

import java.util.List;
import java.util.Optional;

import com.fhzwickau.wissarbeit.dto.AdminDataThemaDTO;
import com.fhzwickau.wissarbeit.dto.CreateThemaDTO;
import com.fhzwickau.wissarbeit.dto.ThemaAdminDTO;
import com.fhzwickau.wissarbeit.dto.ThemaDataDTO;
import com.fhzwickau.wissarbeit.dto.UserPriosDTO;
import com.fhzwickau.wissarbeit.dto.UserThemaDTO;
import com.fhzwickau.wissarbeit.thema.domain.Thema;
import com.fhzwickau.wissarbeit.user.domain.User;

public interface ThemaService {

	public UserThemaDTO getAllThemas(Optional<User> user);

	public boolean savePrios(UserPriosDTO dto, Optional<User> user);

	public List<AdminDataThemaDTO> getAdminDataThemaDTOs();

	public Optional<Thema> getThema(String themaId);

	public ThemaAdminDTO getAllThemaDataForUser(String userId);

	public boolean assignThema(String themaId, String userId);

	public boolean deleteAssignThema(String themaId, String userId);

	public boolean excludeThema(String themaId, String userId);

	public boolean allowThema(String themaId, String userId);

	public boolean createThema(CreateThemaDTO createThema);

	public boolean editThema(String themaId, ThemaDataDTO dto);

	public boolean deleteThema(String themaId);

	public ThemaDataDTO getThemaDataDTO(String themaId);

	public boolean autoAssignThema();
}
