package com.fhzwickau.wissarbeit.thema.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.fhzwickau.wissarbeit.config.BaseEntity;
import com.fhzwickau.wissarbeit.user.domain.User;

@Entity
public class Thema extends BaseEntity<Long> {

	@Column
	private String name;

	@Column(columnDefinition = "LONGTEXT")
	private String description;

	@OneToOne(optional = true)
	private User assignUser;

	public Thema() {
	}

	public Thema(String name, String description) {
		super();
		this.name = name;
		this.description = description;
		this.assignUser = null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getAssignUser() {
		return assignUser;
	}

	public void setAssignUser(User assignUser) {
		this.assignUser = assignUser;
	}

}
