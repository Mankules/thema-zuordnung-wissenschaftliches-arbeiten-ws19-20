package com.fhzwickau.wissarbeit.thema.domain;

public enum Priority {

	FAVORIT, GUT, SCHLECHT, GESPERRT;
}
