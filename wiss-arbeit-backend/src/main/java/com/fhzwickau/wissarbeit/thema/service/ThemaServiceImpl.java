package com.fhzwickau.wissarbeit.thema.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fhzwickau.wissarbeit.algorithmus.HungarianAlgorithm;
import com.fhzwickau.wissarbeit.dto.AdminDataThemaDTO;
import com.fhzwickau.wissarbeit.dto.CreateThemaDTO;
import com.fhzwickau.wissarbeit.dto.ThemaAdminDTO;
import com.fhzwickau.wissarbeit.dto.ThemaAdminDataDTO;
import com.fhzwickau.wissarbeit.dto.ThemaDataDTO;
import com.fhzwickau.wissarbeit.dto.UserPriosDTO;
import com.fhzwickau.wissarbeit.dto.UserPriosDataDTO;
import com.fhzwickau.wissarbeit.dto.UserThemaDTO;
import com.fhzwickau.wissarbeit.dto.UserThemaDataDTO;
import com.fhzwickau.wissarbeit.security.service.CurrentUserUtil;
import com.fhzwickau.wissarbeit.thema.domain.Priority;
import com.fhzwickau.wissarbeit.thema.domain.Thema;
import com.fhzwickau.wissarbeit.thema.domain.ThemaRepository;
import com.fhzwickau.wissarbeit.user.domain.User;
import com.fhzwickau.wissarbeit.user.domain.UserRepository;
import com.fhzwickau.wissarbeit.user.service.UserService;

@Service
public class ThemaServiceImpl implements ThemaService {

	@Autowired
	ThemaRepository themaRepository;
	@Autowired
	UserRepository userRepository;

	@Autowired
	UserService userService;

	/**
	 * This method returns a optional of a thema in the repository.
	 * 
	 * @param themaID, id of the thema to get his object
	 * 
	 * @return Optional<Thema>, Optional of thema in repository
	 */
	@Override
	public Optional<Thema> getThema(String themaId) {
		return themaRepository.findById((Long.valueOf(themaId)));
	}

	/**
	 * This method returns a UserThemaDTO which contains all for the given user
	 * object visible themas and his assigned thema if avaiable.
	 * 
	 * @param Optional<User>, Optional user object
	 * 
	 * @return UserThemaDTO, DTO which contains all necessary data of the user
	 */
	@Override
	public UserThemaDTO getAllThemas(Optional<User> user) {
		List<UserThemaDataDTO> themas = new ArrayList<UserThemaDataDTO>();
		List<Thema> themaliste = new ArrayList<Thema>(themaRepository.findAll());
		ThemaDataDTO assignThema = null;
		if (user.isPresent()) {
			for (int i = 0; i < themaliste.size(); i++) {
				Thema thema = themaliste.get(i);
				if (thema.getAssignUser() == null) {
					if (!(user.get().getExcludeThemas().contains(thema))) {
						if (user.get().containsAbgebenePrio(thema)) {
							themas.add(new UserThemaDataDTO(thema.getId(), thema.getName(), thema.getDescription(),
									user.get().getPriorityForThema(thema).toString()));
						} else {
							themas.add(
									new UserThemaDataDTO(thema.getId(), thema.getName(), thema.getDescription(), null));
						}
					}
				} else {
					if (thema.getAssignUser().equals(user.get()) && user.get().getAssignThema().equals(thema)) {
						assignThema = new ThemaDataDTO(thema.getName(), thema.getDescription());
					}
				}
			}
		}
		return new UserThemaDTO(assignThema, themas);
	}

	/**
	 * This method returns the status of the operation to save all the new
	 * priority's of the given user object.
	 * 
	 * @param Optional<User>, Optional user object
	 * @param UserPriosDTO,   DTO which contains the new priority's for his
	 *                        available themas
	 * 
	 * @return true/false, save of his priority's was complete or not
	 */
	@Override
	public boolean savePrios(UserPriosDTO dto, Optional<User> user) {
		if (user.isPresent()) {
			if (checkThemaIds(dto.getPrios(), user) && checkPriorityEnums(dto.getPrios())) {
				if (userService.savePriosForUser(dto.getPrios(), user)) {
					return true;
				}
				return false;
			}
			return false;
		}
		return false;
	}

	/**
	 * This method returns the status if all the given themaIds in the DTO are
	 * possible of the given user object
	 * 
	 * @param Optional<User>,         Optional user object
	 * @param List<UserPriosDataDTO>, list of UserPriosDataDTO which contains
	 *                                themaIds and priority's
	 * 
	 * @return true/false, all themaIds are OK or not
	 */
	private boolean checkThemaIds(List<UserPriosDataDTO> prios, Optional<User> user) {
		if (user.isPresent()) {
			for (int i = 0; i < prios.size(); i++) {
				if (getThema(prios.get(i).getThemaId()).isPresent()) {
					if (user.get().containsExcludeThema(getThema(prios.get(i).getThemaId()).get())) {
						return false;
					}
				} else {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * This method returns the status if all the given priority's in the DTO are
	 * possible of the given user object
	 * 
	 * @param Optional<User>,         Optional user object
	 * @param List<UserPriosDataDTO>, list of UserPriosDataDTO which contains
	 *                                themaIds and priority's
	 * 
	 * @return true/false, all priority's are OK or not
	 */
	private boolean checkPriorityEnums(List<UserPriosDataDTO> prios) {
		for (int i = 0; i < prios.size(); i++) {
			String prio = prios.get(i).getPrio();
			if (prio.equals(Priority.FAVORIT.toString())) {
			} else {
				if (prio.equals(Priority.GUT.toString())) {
				} else {
					if (prio.equals(Priority.SCHLECHT.toString())) {
					} else {
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * This method returns a list of AdminDataThemaDTO for the Admin to get all the
	 * necessary data of the themas.
	 * 
	 * @return List<AdminDataThemaDTO>, list of AdminDataThemaDTOs
	 */
	@Override
	public List<AdminDataThemaDTO> getAdminDataThemaDTOs() {
		List<AdminDataThemaDTO> themas = new ArrayList<AdminDataThemaDTO>();
		List<Thema> themalist = new ArrayList<Thema>(themaRepository.findAll());
		themalist.stream().forEach(
				thema -> themas.add(new AdminDataThemaDTO(thema.getId(), thema.getName(), thema.getDescription())));
		return themas;
	}

	/**
	 * This method returns a ThemaAdminDTO which contains all the necessary thema
	 * data for the given user object for admin.
	 * 
	 * @return ThemaAdminDTO, DTO with all the data of allow, exclude and assign
	 *         thema
	 */
	@Override
	public ThemaAdminDTO getAllThemaDataForUser(String userId) {
		Optional<User> user = userService.getUserByID(Long.valueOf(userId));
		if (user.isPresent()) {
			List<Thema> themas = new ArrayList<Thema>(themaRepository.findAll());
			List<ThemaAdminDataDTO> allowThemas = new ArrayList<ThemaAdminDataDTO>();
			List<ThemaAdminDataDTO> excludeThemas = new ArrayList<ThemaAdminDataDTO>();
			ThemaAdminDataDTO assignThema = null;
			for (Thema thema : themas) {
				if (thema.getAssignUser() == null) {
					if (user.get().getExcludeThemas().contains(thema)) {
						excludeThemas.add(new ThemaAdminDataDTO(thema.getId(), thema.getName()));
					} else {
						allowThemas.add(new ThemaAdminDataDTO(thema.getId(), thema.getName()));
					}
				} else {
					if (!(thema.getAssignUser() == null) && thema.getAssignUser().equals(user.get())
							&& user.get().getAssignThema().equals(thema)) {
						assignThema = new ThemaAdminDataDTO(thema.getId(), thema.getName());
					}
				}
			}
			return new ThemaAdminDTO(allowThemas, excludeThemas, assignThema);
		}
		return new ThemaAdminDTO();
	}

	/**
	 * This method returns the status of the assignment of the given thema to the
	 * given user.
	 * 
	 * @param userId,  user to assign the thema
	 * @param themaId, thema to assign to user
	 * 
	 * @return true/false, operation was successful or not
	 */
	@Override
	public boolean assignThema(String themaId, String userId) {
		Optional<User> user = userService.getUserByID(Long.valueOf(userId));
		Optional<Thema> thema = getThema(themaId);
		if (user.isPresent() && thema.isPresent() && !(user.get().getExcludeThemas().contains(thema.get()))) {
			if (!(user.get().getAssignThema() == null)) {
				user.get().getAssignThema().setAssignUser(null);
			}
			if (!(thema.get().getAssignUser() == null)) {
				thema.get().getAssignUser().setAssignThema(null);
			}
			user.get().setAssignThema(thema.get());
			thema.get().setAssignUser(user.get());
			themaRepository.flush();
			userRepository.flush();
			return true;
		}
		return false;
	}

	/**
	 * This method returns the status of the removing the assignment of the given
	 * thema to the given user.
	 * 
	 * @param userId,  user to remove the assign the thema
	 * @param themaId, thema to remove the assign to user
	 * 
	 * @return true/false, operation was successful or not
	 */
	@Override
	public boolean deleteAssignThema(String themaId, String userId) {
		Optional<User> user = userService.getUserByID(Long.valueOf(userId));
		Optional<Thema> thema = getThema(themaId);
		if (user.isPresent() && thema.isPresent() && user.get().getAssignThema().equals(thema.get())
				&& thema.get().getAssignUser().equals(user.get())) {
			user.get().setAssignThema(null);
			thema.get().setAssignUser(null);
			themaRepository.flush();
			userRepository.flush();
			return true;
		}
		return false;
	}

	/**
	 * This method returns the status of add the given thema to given user exclude
	 * thema list.
	 * 
	 * @param userId,  user which get the exclude thema
	 * @param themaId, thema to add to the exclude list
	 * 
	 * @return true/false, operation was successful or not
	 */
	@Override
	public boolean excludeThema(String themaId, String userId) {
		Optional<User> user = userService.getUserByID(Long.valueOf(userId));
		Optional<Thema> thema = getThema(themaId);
		if (user.isPresent() && thema.isPresent() && !user.get().containsExcludeThema(thema.get())) {
			user.get().addExcludeThema(thema.get());
			user.get().removeThemaInAbgebenePrios(thema.get());
			userRepository.flush();
			return true;
		}
		return false;
	}

	/**
	 * This method returns the status of allow the given thema to the given user
	 * with removing his entry in the user exclude thema list.
	 * 
	 * @param userId,  user which get allow the thema
	 * @param themaId, thema remove from the exclude list
	 * 
	 * @return true/false, operation was successful or not
	 */
	@Override
	public boolean allowThema(String themaId, String userId) {
		Optional<User> user = userService.getUserByID(Long.valueOf(userId));
		Optional<Thema> thema = getThema(themaId);
		if (user.isPresent() && thema.isPresent() && user.get().containsExcludeThema(thema.get())) {
			user.get().removeExcludeThema(thema.get());
			userRepository.flush();
			return true;
		}
		return false;
	}

	/**
	 * This method returns the status create the new thema out of the given DTO
	 * data.
	 * 
	 * @param creatThemaDTO, data to create new thema
	 * 
	 * @return true/false, creation was successful or not
	 */
	@Override
	public boolean createThema(CreateThemaDTO createThema) {
		if (createThema == null || createThema.getName().isEmpty() || createThema.getDescription().isEmpty()) {
			return false;
		}
		themaRepository.save(new Thema(createThema.getName(), createThema.getDescription()));
		themaRepository.flush();
		return true;
	}

	/**
	 * This method returns the status edit the given thema with the given DTO data.
	 * 
	 * @param ThemaDataDTO, data to edit the given thema
	 * @param themaId,      thema to edit
	 * 
	 * @return true/false, edit was successful or not
	 */
	@Override
	public boolean editThema(String themaId, ThemaDataDTO dto) {
		Optional<Thema> thema = getThema(themaId);
		if (thema.isPresent() && !dto.getName().isEmpty() && !dto.getDescription().isEmpty()) {
			thema.get().setName(dto.getName());
			thema.get().setDescription(dto.getDescription());
			themaRepository.flush();
			return true;
		}
		return false;
	}

	/**
	 * This method returns the ThemaDataDTO of a given themaId.
	 * 
	 * @param themaId, to get the themaDataDTO
	 * 
	 * @return ThemaDataDTO, contains the name and description of the thema
	 */
	@Override
	public ThemaDataDTO getThemaDataDTO(String themaId) {
		Optional<Thema> thema = getThema(themaId);
		if (thema.isPresent()) {
			return new ThemaDataDTO(thema.get().getName(), thema.get().getDescription());
		}
		return new ThemaDataDTO();
	}

	/**
	 * This method returns the status delete the given themaId.
	 * 
	 * @param themaId, to get the thema to remove
	 * 
	 * @return true/false, delete was successful or not
	 */
	@Override
	public boolean deleteThema(String themaId) {
		Optional<Thema> thema = getThema(themaId);
		if (thema.isPresent()) {
			if (!(thema.get().getAssignUser() == null)) {
				thema.get().getAssignUser().setAssignThema(null);
				userRepository.flush();
			}
			List<User> users = new ArrayList<User>(userRepository.findAll());
			for (int i = 0; i < users.size(); i++) {
				User user = users.get(i);
				user.removeThemaInAbgebenePrios(thema.get());
				user.removeExcludeThema(thema.get());
				userRepository.flush();
			}
			themaRepository.delete(thema.get());
			themaRepository.flush();
			return true;
		}
		return false;
	}

	/**
	 * This method returns the status of the assignment of the alle the remaining
	 * themas to the users. It calls the HungarianAlgorithmus class to get the best
	 * possible decision.
	 * 
	 * @return true/false, automatic assignment of the thema to the users was
	 *         successful or not
	 */
	@Override
	public boolean autoAssignThema() {
		List<User> rawusers = new ArrayList<User>(userRepository.findAll());
		List<Thema> rawthemas = new ArrayList<Thema>(themaRepository.findAll());
		if (rawusers.contains(CurrentUserUtil.getCurrentUser(userService).get())) {
			rawusers.remove(CurrentUserUtil.getCurrentUser(userService).get());
		}
		List<User> users = new ArrayList<User>();
		List<Thema> themas = new ArrayList<Thema>();

		// nicht abgegebene aber m�gliche Prios mit Schlecht f�llen und excludePrios mit
		// Gesperrt
		for (User user : rawusers) {
			for (Thema thema : rawthemas) {
				if (user.getAssignThema() == null && thema.getAssignUser() == null && !user.containsExcludeThema(thema)
						&& !user.containsAbgebenePrio(thema)) {
					user.addAbgegebenePrio(thema, Priority.SCHLECHT);
				}
				if (user.getAssignThema() == null && thema.getAssignUser() == null && user.containsExcludeThema(thema)
						&& !user.containsAbgebenePrio(thema)) {
					user.addAbgegebenePrio(thema, Priority.GESPERRT);
				}
			}
		}

		// themas f�llen
		for (Thema thema : rawthemas) {
			if (thema.getAssignUser() == null) {
				themas.add(thema);
			}
		}

		// users f�llen
		for (User user : rawusers) {
			if (user.getAssignThema() == null) {
				for (Thema thema : rawthemas) {
					if (!(thema.getAssignUser() == null)) {
						if (user.containsAbgebenePrio(thema)) {
							user.removeThemaInAbgebenePrios(thema);
						}
					}
				}
				if (!users.contains(user)) {
					users.add(user);
				}
			}
		}

		System.out.println("usersl�nge:" + users.size());
		System.out.println("themal�nge:" + themas.size());

		// �berpr�ft ob genug Themen f�r die user vorhanden sind
		if (themas.size() < users.size()) {
			return false;
		}

		double[][] matrix = new double[users.size()][themas.size()];

		for (int i = 0; i < users.size(); i++) {
			User user = users.get(i);
			// System.out.println(user.getNickname() + "Daten:");
			List<Thema> themaKeys = new ArrayList<Thema>(user.getAbgebenePrios().keySet());
			List<Priority> prios = new ArrayList<Priority>(user.getAbgebenePrios().values());
			// prios.stream().forEach(prio -> System.out.println(prio.toString()));
			// themaKeys.stream().forEach(thema -> System.out.println(thema.getName()));

			for (int k = 0; k < themaKeys.size(); k++) {
				if (prios.get(k).equals(Priority.FAVORIT)) {
					matrix[i][k] = 1;
				} else {
					if (prios.get(k).equals(Priority.GUT)) {
						matrix[i][k] = 2;
					} else {
						if (prios.get(k).equals(Priority.SCHLECHT)) {
							matrix[i][k] = 3;
						} else {
							if (prios.get(k).equals(Priority.GESPERRT)) {
								matrix[i][k] = 10;
							}
						}
					}
				}
			}
		}

		StringBuffer results = new StringBuffer();
		String separator = "|";
		for (int e = 0; e < users.size(); e++) {
			results.append('[');
			for (int r = 0; r < themas.size(); r++) {
				results.append(matrix[e][r]);
				results.append(separator);
			}
			results.append(']');
			results.append("\r\n");
		}
		System.out.println(results);

		if (matrix.length > 0) {
			HungarianAlgorithm alg = new HungarianAlgorithm(matrix);
			int[] ergebnis = alg.execute();

			for (int t = 0; t < ergebnis.length; t++) {
				if (users.get(t).containsExcludeThema(themas.get(ergebnis[t]))) {
					System.out.println(users.get(t).getName() + " " + users.get(t).getSurname()
							+ " wurde ein ausgeschlossenes Thema zugewiesen, �berpr�fen sie den Themen und User bitte!");
					return false;
				}
			}

			for (int a = 0; a < ergebnis.length; a++) {
				users.get(a).setAssignThema(themas.get(ergebnis[a]));
				themas.get(ergebnis[a]).setAssignUser(users.get(a));

				StringBuffer output = new StringBuffer();
				output.append((users.get(a).getName() + " " + users.get(a).getSurname()));
				output.append((" hat " + themas.get(ergebnis[a]).getName() + " zugewiesen bekommen"));
				System.out.println(output);
				System.out.println(ergebnis[a]);
			}
			themaRepository.flush();
			userRepository.flush();
			return true;
		}
		return false;
	}
}
