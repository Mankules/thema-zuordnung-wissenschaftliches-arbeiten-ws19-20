package com.fhzwickau.wissarbeit.security.service;

import java.util.Optional;

import org.springframework.security.core.context.SecurityContextHolder;

import com.fhzwickau.wissarbeit.user.domain.User;
import com.fhzwickau.wissarbeit.user.service.UserService;

public class CurrentUserUtil {

	public static Optional<User> getCurrentUser(UserService userService) {
		String nameCurrentUser = SecurityContextHolder.getContext().getAuthentication().getName();
		return userService.getUserByName(nameCurrentUser);
	}

}
