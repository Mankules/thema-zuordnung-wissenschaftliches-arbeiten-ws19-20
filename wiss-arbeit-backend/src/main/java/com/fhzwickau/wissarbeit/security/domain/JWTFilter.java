package com.fhzwickau.wissarbeit.security.domain;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.fhzwickau.wissarbeit.security.service.TokenService;
import com.fhzwickau.wissarbeit.user.domain.User;
import com.fhzwickau.wissarbeit.user.service.UserService;

public class JWTFilter extends BasicAuthenticationFilter {

	private TokenService tokenService;

	private UserService userService;

	public JWTFilter(UserService userService, TokenService tokenService, AuthenticationManager authenticationManager) {
		super(authenticationManager);
		this.tokenService = tokenService;
		this.userService = userService;
	}

	public boolean allowRequestWithoutToken(HttpServletRequest request) {
		if (request.getRequestURI().contains("/register") || request.getRequestURI().contains("/login")
				|| request.getRequestURI().contains("/h2")) {
			// h2 f�r Konsolenansicht
			return true;
		}
		return false;
	}

	@Override
	public void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest request = req;
		HttpServletResponse response = res;
		String token = (request).getHeader("Authorization");
		if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
			response.sendError(HttpServletResponse.SC_OK, "success");
			return;
		}

		if (allowRequestWithoutToken(request)) {
			response.setStatus(HttpServletResponse.SC_OK);
			filterChain.doFilter(request, response);
		} else {
			if (token == null) {
				filterChain.doFilter(req, res);
				return;
			} else if (!tokenService.isTokenValid(token)) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token is not valid");
			} else {
				String userName = (tokenService.getUserNameFromToken(token));
				Optional<User> user = userService.getUserByName(userName);
				if (user.isPresent()) {
					UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
							userName, null, AuthorityUtils.createAuthorityList(user.get().getRole().toString()));
					SecurityContextHolder.getContext().setAuthentication(authentication);
					request.setAttribute("userName", userName);
					filterChain.doFilter(req, res);
				} else {
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "User is not valid");
				}

			}
		}
	}
}