package com.fhzwickau.wissarbeit.security.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fhzwickau.wissarbeit.config.BaseEntity;

@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class DataProtectionMessage extends BaseEntity<Long> {

	@Column(length = 100000)
	private String message = "";

	public DataProtectionMessage() {

	}

	public DataProtectionMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
