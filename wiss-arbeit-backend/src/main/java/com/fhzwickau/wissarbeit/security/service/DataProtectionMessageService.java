package com.fhzwickau.wissarbeit.security.service;

public interface DataProtectionMessageService {

	public boolean initDataProtectionMessage(Long dpmId);

	public boolean setDataProtectionMessage(String text);

	public String getDataProtectionMessage();
}
