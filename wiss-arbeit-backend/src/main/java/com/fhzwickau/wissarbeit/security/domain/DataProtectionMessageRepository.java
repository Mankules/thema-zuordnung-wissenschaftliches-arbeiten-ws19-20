package com.fhzwickau.wissarbeit.security.domain;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DataProtectionMessageRepository extends JpaRepository<DataProtectionMessage, Long> {

}
