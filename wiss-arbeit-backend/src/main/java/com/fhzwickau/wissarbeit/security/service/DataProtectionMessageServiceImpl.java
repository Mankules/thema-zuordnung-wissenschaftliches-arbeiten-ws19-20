package com.fhzwickau.wissarbeit.security.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.fhzwickau.wissarbeit.security.domain.DataProtectionMessage;
import com.fhzwickau.wissarbeit.security.domain.DataProtectionMessageRepository;

@Service
public class DataProtectionMessageServiceImpl implements DataProtectionMessageService {

	@Autowired
	DataProtectionMessageRepository dpmRepo;

	@Value("classpath:DataProtectionText.txt")
	private Resource txtResource;

	private Logger logger = LoggerFactory.getLogger(DataProtectionMessageServiceImpl.class);

	@Override
	public boolean initDataProtectionMessage(Long dpmId) {
		DataProtectionMessage dpm = dpmRepo.getOne(dpmId);
		if (dpm != null) {
			InputStream resource;
			try {
				resource = new ClassPathResource("DataProtectionText.txt").getInputStream();
				try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource))) {
					String text = reader.lines().collect(Collectors.joining("\n"));
					if (text != null) {
						dpm.setMessage(text);
						dpmRepo.save(dpm);
						return true;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public boolean setDataProtectionMessage(String text) {
		logger.info("DATAPROTECTIONMESSEGESERVICE COUNT: " + dpmRepo.count());
		if (dpmRepo.count() == 0) {
			return false;
		} else {
			DataProtectionMessage dpm = dpmRepo.findAll().get(0);
			dpm.setMessage(text);
			dpmRepo.save(dpm);
			File file;
			try {
				file = new ClassPathResource("DataProtectionText.txt").getFile();

				if (!file.exists()) {
					file.createNewFile();
				}
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(text);
				bw.close();
				return true;
			} catch (Exception e) {

			}
		}
		return false;
	}

	@Override
	public String getDataProtectionMessage() {
		// System.out.println("getDataProtectionMessage aufgerufen");
		// System.out.println(dpmRepo.toString());
		// System.out.println(dpmRepo.count());
		if (dpmRepo.count() == 0) {
			return "[No Text]";
		} else {
			return dpmRepo.findAll().get(0).getMessage();
		}
	}
}
