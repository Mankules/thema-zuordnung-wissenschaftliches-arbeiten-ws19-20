package com.fhzwickau.wissarbeit.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.fhzwickau.wissarbeit.user.domain.CurrentUser;
import com.fhzwickau.wissarbeit.user.domain.User;
import com.fhzwickau.wissarbeit.user.service.UserService;

@Service
public class CurrentUserDetailsService implements UserDetailsService {

	@Autowired
	UserService userService;

	@Override
	public CurrentUser loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = null;
		user = userService.getUserByName(username).orElseThrow(
				() -> new UsernameNotFoundException("User with username= " + username + " cannot be not found"));

		return new CurrentUser(user);
	}

}
