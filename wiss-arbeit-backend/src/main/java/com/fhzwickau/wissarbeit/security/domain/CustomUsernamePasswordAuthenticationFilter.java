package com.fhzwickau.wissarbeit.security.domain;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fhzwickau.wissarbeit.dto.AuthUserDTO;
import com.fhzwickau.wissarbeit.user.domain.User;
import com.fhzwickau.wissarbeit.user.service.UserService;

public class CustomUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	public static final String TOKEN_SECRET = "s4T2zOIWHMM1sxq";
	private AuthenticationManager authenticationManager;
	private UserService userService;
	private Logger logger = LoggerFactory.getLogger(CustomUsernamePasswordAuthenticationFilter.class);

	public CustomUsernamePasswordAuthenticationFilter(AuthenticationManager authenticationManager,
			UserService userService) {
		this.authenticationManager = authenticationManager;
		this.userService = userService;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		String userName = null;
		try {
			BufferedReader reader = request.getReader();
			StringBuffer sb = new StringBuffer();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			String parsedReq = sb.toString();
			if (parsedReq != null) {
				ObjectMapper mapper = new ObjectMapper();
				AuthReq authReq = mapper.readValue(parsedReq, AuthReq.class);
				userName = authReq.getUsername();
				return authenticationManager.authenticate(
						new UsernamePasswordAuthenticationToken(authReq.getUsername(), authReq.getPassword()));
			}
		} catch (BadCredentialsException e) {
			try {
				if (userName != null) {
					logger.info("Der User " + userName
							+ " konnte sich nicht einloggen, da entweder password oder login falsch waren!");
				} else {
					logger.info("Ein User versuchte sich einzuloggen. Der name ist aber null");
				}
				response.sendError(401, "Bad Credentials");
			} catch (IOException e1) {
				e1.printStackTrace();
				throw new InternalAuthenticationServiceException(
						"Etwas beim Senden des fehlerhaften Logins ist fehlgeschlagen!");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			throw new InternalAuthenticationServiceException("Failed to parse authentication request body");
		}
		return null;
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		String token = "";

		try {
			Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
			token = JWT.create().withClaim("userName", authResult.getName()).withClaim("createdAt", new Date())
					.sign(algorithm);
		} catch (UnsupportedEncodingException exception) {
			exception.printStackTrace();
			// log WRONG Encoding message
		} catch (JWTCreationException exception) {
			exception.printStackTrace();
			// log Token Signing Failed
		}
		PrintWriter writer = response.getWriter();
		User user = userService.getUserByName(authResult.getName()).get();
		AuthUserDTO userDTO = new AuthUserDTO(user.getId(), user.getName(), user.getSurname(), user.getRole(),
				user.getNickname(), token, user.getDataProtectionAccepted());
		ObjectMapper m = new ObjectMapper();
		writer.write(m.writeValueAsString(userDTO));
		writer.flush();
	}

	public static class AuthReq {
		String username;
		String password;

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

	}

}