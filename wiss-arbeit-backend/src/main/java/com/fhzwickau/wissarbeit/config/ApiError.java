package com.fhzwickau.wissarbeit.config;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

public class ApiError {

	private HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

	private String message = "Error";

	private List<String> subMessages = new ArrayList<>();

	private LocalDateTime timeStamp = LocalDateTime.now();

	/**
	 * @param status
	 * @param message
	 */
	public ApiError(HttpStatus status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	/**
	 * @param status
	 * @param message
	 * @param subMessages
	 */
	public ApiError(HttpStatus status, String message, List<String> subMessages) {
		super();
		this.status = status;
		this.message = message;
		this.subMessages = subMessages;
	}

	/**
	 * @return the status
	 */
	public HttpStatus getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the subMessages
	 */
	public List<String> getSubMessages() {
		return subMessages;
	}

	/**
	 * @param subMessages the subMessages to set
	 */
	public void setSubMessages(List<String> subMessages) {
		this.subMessages = subMessages;
	}

	/**
	 * @return the timeStamp
	 */
	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}

	/**
	 * @param timeStamp the timeStamp to set
	 */
	public void setTimeStamp(LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	}

}
