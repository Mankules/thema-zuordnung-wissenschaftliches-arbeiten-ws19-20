package com.fhzwickau.wissarbeit.config;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fhzwickau.wissarbeit.security.service.CurrentUserUtil;
import com.fhzwickau.wissarbeit.user.domain.User;
import com.fhzwickau.wissarbeit.user.service.UserService;

@RestControllerAdvice
public class GlobalErrorControllerAdvice {

	@Autowired
	private UserService userService;

	private Logger logger = LoggerFactory.getLogger(GlobalErrorControllerAdvice.class);

	@ExceptionHandler(GeneralErrorException.class)
	public ResponseEntity<ApiError> handleGeneralErrorException(GeneralErrorException e) {
		e.printStackTrace();
		return new ResponseEntity<ApiError>( // "Request must have body! " +
				e.getApiError(), e.getApiError().getStatus());
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ApiError> handleOtherExceptions(Exception e) {
		e.printStackTrace();
		return new ResponseEntity<ApiError>( // "Internal server error! " + e.getMessage()
				new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Der Server hat einen internen Fehler festgestellt!"),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(AccessDeniedException.class)
	public ResponseEntity<ApiError> handleAccessDenied(AccessDeniedException e) {

		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		List<String> header = new ArrayList<>();
		Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			header.add(headerName + ":" + request.getHeader(headerName) + "\n");
		}
		List<String> params = new ArrayList<>();
		Enumeration<String> paramsE = request.getParameterNames();
		while (paramsE.hasMoreElements()) {
			String paramName = paramsE.nextElement();
			params.add(paramName + ":" + request.getParameter(paramName) + "\n");
		}

		logger.error(
				"Access Denied! \nMethod: " + request.getMethod() + "\nHeader:\n " + header + "\nBody:\n" + params);

		Optional<User> userOpt = CurrentUserUtil.getCurrentUser(userService);
		if (userOpt.isPresent()) {
			logger.error(
					"Der User " + userOpt.get().getNickname() + " versuchte auf eine gesperrte Resource zuzugreifen!");
		}
		return new ResponseEntity<ApiError>( // "Internal server error! " + e.getMessage()
				new ApiError(HttpStatus.FORBIDDEN, "Sie haben nicht die Rechte um auf diese Resource zuzugreifen!"),
				HttpStatus.FORBIDDEN);
	}

}
