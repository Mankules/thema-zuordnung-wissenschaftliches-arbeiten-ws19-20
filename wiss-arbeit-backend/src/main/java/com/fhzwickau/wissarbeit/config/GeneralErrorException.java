package com.fhzwickau.wissarbeit.config;

public class GeneralErrorException extends Exception {

	private ApiError apiError;

	public GeneralErrorException(ApiError apiError) {
		// Call constructor of parent Exception
		super(apiError.getMessage());
		this.apiError = apiError;
	}

	/**
	 * @return the apiError
	 */
	public ApiError getApiError() {
		return apiError;
	}

	/**
	 * @param apiError the apiError to set
	 */
	public void setApiError(ApiError apiError) {
		this.apiError = apiError;
	}

}
