package com.fhzwickau.wissarbeit.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.fhzwickau.wissarbeit.security.domain.DataProtectionMessage;
import com.fhzwickau.wissarbeit.security.domain.DataProtectionMessageRepository;
import com.fhzwickau.wissarbeit.security.service.DataProtectionMessageService;
import com.fhzwickau.wissarbeit.thema.domain.Priority;
import com.fhzwickau.wissarbeit.thema.domain.Thema;
import com.fhzwickau.wissarbeit.thema.domain.ThemaRepository;
import com.fhzwickau.wissarbeit.user.domain.Role;
import com.fhzwickau.wissarbeit.user.domain.User;
import com.fhzwickau.wissarbeit.user.domain.UserRepository;

@Configuration
public class InitializeDB {

	@Autowired
	private ThemaRepository themaRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private DataProtectionMessageRepository dpmRepo;
	@Autowired
	private DataProtectionMessageService dpmService;

	@PostConstruct
	public void init() {

		DataProtectionMessage dpm = new DataProtectionMessage();
		dpmRepo.save(dpm);
		dpmService.initDataProtectionMessage(dpm.getId());

		/*
		 * Thema thema1 = new Thema("Qualit�t von Lehrvideos",
		 * "Suchen Sie zu einem Thema der Informatik oder Mathematik, das Sie ohnehin bearbeiten m�ssen, auf YouTube oder aus anderen Quellen mehrere Lehrvideos in deutscher\r\n"
		 * + "oder englischer Sprache.\r\n" +
		 * "Bewerten Sie, welche Videos f�r das Lernen gut und welche schlecht geeignet sind.\r\n"
		 * +
		 * "Analysieren Sie dann, was dazu f�hrt, dass ein Lehrvideo als besonders n�tzlich oder als\r\n"
		 * + "besonders unbrauchbar wahrgenommen wird.\r\n" + "Literatur:\r\n" +
		 * "Vanessa Rau, Dirk Niem�ller, Lisa Berkemeier: Ist weniger mehr? � Designprinzipien f�r\r\n"
		 * + "Virtual Reality Training aus kognitionspsychologischer Sicht,\r\n" +
		 * "HMD Praxis der Wirtschaftsinformatik, Ausgabe 4/2019\r\n" +
		 * "(Das soll nat�rlich nur ein Anfang sein � suchen Sie selbst weitere Literatur!)"
		 * ); Thema thema2 = new Thema("�hnlichkeit von gesprochenen Zeichenketten",
		 * "Erstellen Sie eine �bersicht �ber vorhandene Software, die ein Ma� f�r die �hnlichkeit\r\n"
		 * +
		 * "zwischen Zeichenketten berechnet und dabei misst, wie �hnlich diese Zeichenketten als\r\n"
		 * + "gesprochene W�rter sind.\r\n" +
		 * "Beispiel: Die Zeichenketten �sehen� und �Seen� sollen als �hnlicher bewertet werden als\r\n"
		 * + "�sehen� und �gehen�"); Thema thema3 = new
		 * Thema("Self-Assesment im Informatikstudium",
		 * "Vergleichen Sie im Web verf�gbare Self-Assesments f�r Absolventen, die sich f�r eine\r\n"
		 * + "Studium der Informatik interessieren.\r\n" +
		 * "Analysieren Sie die gestellten Fragen und untersuchen Sie, welche Kenntnisse, Erwartungen oder Pers�nlichkeitsmerkmale mit den Fragen abgefragt werden. Diskutieren Sie\r\n"
		 * +
		 * "weiterhin, wie gut die Tests die Testteilnehmer motivieren, den Test auch tats�chlich\r\n"
		 * + "abzuschlie�en.\r\n" +
		 * "Entwerfen Sie dann aufbauend auf diesen Erkenntnissen eine Menge von Fragen die\r\n"
		 * +
		 * "geeignet ist, m�gliche zuk�nftige Informatik-Studenten an der WHZ dar�ber zu informieren, ob das Informatikstudium f�r sie das Richtige ist.\r\n"
		 * + "Quelle: http://osa-portal.de/"); Thema thema4 = new
		 * Thema("Vergleich von Pr�fziffernberechnungsmethoden",
		 * "F�r die Berechnung von Pr�fziffern gibt es eine gro�e Zahl verschiedener Verfahren. Die\r\n"
		 * +
		 * "bei deutschen Banken verwendeten Verfahren sind einander sehr �hnlich und beruhen in\r\n"
		 * + "der Regel auf Restklassen-Addition modulo 10 oder 11 [1].\r\n" +
		 * "Im Vorjahr hat eine Studentin bereits ein Programm erstellt, das die Qualit�t verschiedener Pr�fziffern-Berechnungsmethoden vergleicht.\r\n"
		 * +
		 * "Grundidee daf�r war es, eine gro�e Anzahl zuf�llige Kontonummern zu erzeugen (zun�chst ohne Pr�fziffer). Dann wurde mit jedem Verfahren die zugeh�rige Pr�fziffer berechnet. Man erh�lt damit also die vollst�ndigen Kontonummern (mit Pr�fziffer).\r\n"
		 * +
		 * "Sodann wurden �Tippfehler� in die vollst�ndigen Kontonummern eingef�gt. Tippfehler\r\n"
		 * + "k�nnen sein:\r\n" +
		 * "a) einzelne falsch geschriebene Ziffern (das kann durchaus auch die Pr�fziffer sein)\r\n"
		 * + "b) das Vertauschen zweier benachbarter Ziffern\r\n" +
		 * "c) das Vertauschen von zwei beliebigen Ziffern\r\n" +
		 * "Nun wurde f�r die verschiedenen Verfahren gepr�ft, wie gro� der Anteil der durch die\r\n"
		 * +
		 * "Pr�fziffer erkannten Fehler in jeder der drei Klassen ist. Gepr�ft wird also, ob das Verfahren trotz einem Tippfehler feststellen w�rde, dass die Pr�fziffer zu den ersten Stellen\r\n"
		 * + "der Kontonummer passt.\r\n" + "Ihre Aufgaben:\r\n" +
		 * "F�gen Sie nun dem Programm weitere Verfahren zur Pr�fziffernberechnungsmethode\r\n"
		 * +
		 * "nach dem Prinzip der zyklischen Redundanzpr�fung hinzu. Wiederholen Sie die vergleichenden Tests der Qualit�t der Verfahren.\r\n"
		 * +
		 * "Verbessern Sie au�erdem das vorhandene Programm, indem Sie mehrfach vorkommende Codeteile sinnvoll zusammenfassen.\r\n"
		 * +
		 * "In Ihrem Vortrag gehen Sie bitte auf die Grundideen der Verfahren zur Pr�fziffernberechnung ein (einmal per Restklassenaddition, zum anderen per zyklischer Redundanzpr�fung). Erl�utern und begr�nden Sie Vor- und Nachteile der einzelnen Verfahren.\r\n"
		 * + "Quellen:\r\n" + "[1] Beschreibung der Berechnungsverfahren, im OPAL\r\n" +
		 * "[2] das im Vorjahr erstellte Programm, im OPAL\r\n" +
		 * "http://www.pruefziffernberechnung.de"); Thema thema5 = new
		 * Thema("Werkzeug�bersicht zur Suche in digitalisierter Literatur",
		 * "Erstellen Sie eine Werkzeug�bersicht zu Werkzeugen, die die Suche in digitalisierten\r\n"
		 * +
		 * "Buch- und Zeitschriftenbest�nden erm�glichen. Suchkriterien sollen variabel kombiniert\r\n"
		 * + "werden k�nnen und eine Suche erlauben nach:\r\n" +
		 * "Ver�ffentlichungsort, Ver�ffentlichungsjahr, Autor in Kombination mit\r\n" +
		 * "Volltextsuche und\r\n" +
		 * "einer Suche nach manuell vergebenen Schlagworten, wobei logische Verkn�pfungen\r\n"
		 * +
		 * "m�glich sein sollen (z. B.: Suche nach Artikeln, die mit �Fu�ball-WM�, jedoch nicht mit\r\n"
		 * + "�U-18� verschlagwortet sind).\r\n" +
		 * "Bei der Definition von Schlagworten soll es m�glich sein, die �ist-ein�-Beziehung zu\r\n"
		 * +
		 * "modellieren. Zum Beispiel soll es m�glich sein, zu sagen, dass �Roggen� und �Weizen�\r\n"
		 * +
		 * "beide �Getreide� sind. Eine Suche nach �Getreide� soll dann auch Artikel finden, die mit\r\n"
		 * + "�Weizen� verschlagwortet worden.\r\n" +
		 * "Ebenso sollen Beziehungen zwischen einzelnen Literaturbeitr�gen ber�cksichtigt werden (Beispiele: �ist eine �bersetzung von�, �ist eine Kurzfassung von�, �ist eine Berichtigung zu�, etc.)\r\n"
		 * +
		 * "Schlie�en Sie insbesondere (aber nicht ausschlie�lich) die Werkzeuge Solr und\r\n"
		 * + "Elasticsearch in Ihre Untersuchung ein.");
		 * 
		 * themaRepository.save(thema1); themaRepository.save(thema2);
		 * themaRepository.save(thema3); themaRepository.save(thema4);
		 * themaRepository.save(thema5);

		 *String passHash = new BCryptPasswordEncoder(10).encode("123");
		 */
		User admin = new User("Adminvorname", "Adminnachname", Role.ADMIN, "admin",
				new BCryptPasswordEncoder(10).encode("12345"));

		/*
		 * User student1 = new User("Max", "Jones", Role.STUDENT, "student1", passHash);
		 * User student2 = new User("Sabine", "Wilkins", Role.STUDENT, "student2",
		 * passHash); User student3 = new User("Robert", "Soloman", Role.STUDENT,
		 * "student3", passHash); User student4 = new User("Michael", "Gilbert",
		 * Role.STUDENT, "student4", passHash); User student5 = new User("Samuel ",
		 * "Davila", Role.STUDENT, "student5", passHash);
		 */

		userRepository.save(admin);
		/*
		 * userRepository.save(student1); userRepository.save(student2);
		 * userRepository.save(student3); userRepository.save(student4);
		 * userRepository.save(student5);
		 */
	}
}
