package com.fhzwickau.wissarbeit.user.domain;

public enum Role {

	STUDENT, ADMIN;
}
