package com.fhzwickau.wissarbeit.user.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fhzwickau.wissarbeit.config.BaseEntity;
import com.fhzwickau.wissarbeit.thema.domain.Priority;
import com.fhzwickau.wissarbeit.thema.domain.Thema;

@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class User extends BaseEntity<Long> {

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private String surname;

	@Column
	@Enumerated(EnumType.STRING)
	private Role role;

	@Column(nullable = false, unique = true, length = 200)
	private String nickname;

	@JsonIgnore
	@Column(nullable = false)
	private String passwordHash;

	@Column(nullable = false)
	private boolean dataProtectionAccepted = false;

	@OneToOne(optional = true)
	private Thema assignThema;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Thema> excludeThemas = new ArrayList<>();

	@ElementCollection(targetClass = Enum.class)
	@JoinTable(name = "prios")
	private Map<Thema, Priority> abgebenePrios = new HashMap<>();

	public User() {
		super();
		abgebenePrios = new HashMap<>();
		excludeThemas = new ArrayList<>();

	}

	public User(String name, String surname, Role role, String nickname, String passwordHash) {
		this();
		this.name = name;
		this.surname = surname;
		this.role = role;
		this.nickname = nickname;
		this.passwordHash = passwordHash;
		this.assignThema = null;
	}

	/**
	 * This method adds the given thema object and his priority to the map. It
	 * checks if there is already a entry and replace it if necessary.
	 * 
	 * @param thema,    Thema object to add
	 * @param priority, Priority enum for the thema
	 */
	public void addAbgegebenePrio(Thema thema, Priority priority) {
		if (abgebenePrios.containsKey(thema)) {
			abgebenePrios.replace(thema, priority);
		} else {
			abgebenePrios.put(thema, priority);
		}

	}

	/**
	 * This method checks if there is already a entry for the thema object in the
	 * map.
	 * 
	 * @param thema, Thema object to check existence
	 * 
	 * @return true/false, answer of the question
	 */
	public boolean containsAbgebenePrio(Thema thema) {
		return abgebenePrios.containsKey(thema);
	}

	/**
	 * This method checks if the given thema object is present in the map and delete
	 * his entry in the map.
	 * 
	 * @param thema, Thema object to remove from map
	 */
	public void removeThemaInAbgebenePrios(Thema thema) {
		if (abgebenePrios.containsKey(thema)) {
			abgebenePrios.remove(thema);
		}
	}

	/**
	 * This method checks if the given thema object is present in the map and
	 * returns his priority for it.
	 * 
	 * @param thema, Thema object to get his priority
	 * @return Priority, the Priority for the thema object
	 */
	public Priority getPriorityForThema(Thema thema) {
		if (abgebenePrios.containsKey(thema)) {
			return abgebenePrios.get(thema);
		}
		return null;
	}

	/**
	 * This method adds the given thema object to the list of exclude themas.
	 * 
	 * @param thema, Thema object to add to exclude thema list
	 */
	public void addExcludeThema(Thema thema) {
		excludeThemas.add(thema);
	}

	/**
	 * This method removes the given thema object from the list of exclude themas.
	 * 
	 * @param thema, Thema object to remove from exclude themas list
	 * @return true/false, delete was successful or not
	 */
	public boolean removeExcludeThema(Thema thema) {
		return excludeThemas.remove(thema);
	}

	/**
	 * This method checks if the given thema object is present in the list of
	 * exclude themas.
	 * 
	 * @param thema, Thema object to get his present in exclude themas list
	 * @return true/false, exclude themas contains the thema object
	 */
	public boolean containsExcludeThema(Thema thema) {
		if (excludeThemas.contains(thema)) {
			return true;
		}
		return false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	@JsonIgnore
	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public boolean getDataProtectionAccepted() {
		return dataProtectionAccepted;
	}

	public void setDataProtectionAccepted(boolean dataProtectionAccepted) {
		this.dataProtectionAccepted = dataProtectionAccepted;
	}

	public Map<Thema, Priority> getAbgebenePrios() {
		return abgebenePrios;
	}

	public void setAbgebenePrios(Map<Thema, Priority> abgebenePrios) {
		this.abgebenePrios = abgebenePrios;
	}

	public Thema getAssignThema() {
		return assignThema;
	}

	public void setAssignThema(Thema assignThema) {
		this.assignThema = assignThema;
	}

	public List<Thema> getExcludeThemas() {
		return excludeThemas;
	}

	public void setExcludeThemas(List<Thema> excludeThemas) {
		this.excludeThemas = excludeThemas;
	}
}
