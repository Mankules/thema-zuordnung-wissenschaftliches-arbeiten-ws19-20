package com.fhzwickau.wissarbeit.user.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.fhzwickau.wissarbeit.dto.AdminDataDTO;
import com.fhzwickau.wissarbeit.dto.AdminDataUserDTO;
import com.fhzwickau.wissarbeit.dto.UserPriosDataDTO;
import com.fhzwickau.wissarbeit.thema.domain.Priority;
import com.fhzwickau.wissarbeit.thema.domain.Thema;
import com.fhzwickau.wissarbeit.thema.domain.ThemaRepository;
import com.fhzwickau.wissarbeit.thema.service.ThemaService;
import com.fhzwickau.wissarbeit.user.domain.Role;
import com.fhzwickau.wissarbeit.user.domain.User;
import com.fhzwickau.wissarbeit.user.domain.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;
	@Autowired
	ThemaRepository themaRepository;

	@Autowired
	ThemaService themaService;

	/**
	 * This method creates and returns the AdminDataDTO. It calls the ThemaService
	 * to get the AdminDataThemaDTO for the Thema data and his own method
	 * AdminDataUserDTO for the user data.
	 * 
	 * @return AdminDataDTO
	 */
	@Override
	public AdminDataDTO getAdminDataDTO() {
		return new AdminDataDTO(themaService.getAdminDataThemaDTOs(), getAdminDataUserDTOs());
	}

	/**
	 * This method creates and returns the list of the AdminDataUserDTO.
	 * 
	 * @return List<AdminDataUserDTO>, list of all the user data for the
	 *         AdminDataDTO
	 */
	private List<AdminDataUserDTO> getAdminDataUserDTOs() {
		List<AdminDataUserDTO> users = new ArrayList<AdminDataUserDTO>();
		List<User> userlist = new ArrayList<User>(userRepository.findAll());
		for (int i = 0; i < userlist.size(); i++) {
			User user = userlist.get(i);
			if (!(user.getRole() == Role.ADMIN)) {
				if (user.getAssignThema() == null) {
					users.add(new AdminDataUserDTO(user.getId(), user.getName(), user.getSurname(), user.getNickname(),
							user.getDataProtectionAccepted(), null));
				} else {
					users.add(new AdminDataUserDTO(user.getId(), user.getName(), user.getSurname(), user.getNickname(),
							user.getDataProtectionAccepted(), user.getAssignThema().getName()));
				}
			}
		}
		return users;
	}

	/**
	 * This method adds the given User to the Repository.
	 * 
	 * @param user, user to add
	 */
	@Override
	public void addUser(User user) {
		userRepository.save(user);
	}

	/**
	 * This method deletes the given user by find him with his id and delete his
	 * reference to his possible assign thema object.
	 * 
	 * @param userId, user to remove
	 * @return true/false, delete operation was successful or not
	 */
	@Override
	public boolean removeUser(String userId) {
		if (userId.isEmpty() || userId == null) {
			return false;
		}
		Optional<User> user = getUserByID(Long.valueOf(userId));
		if (user.isPresent()) {
			if (!(user.get().getAssignThema() == null)) {
				user.get().getAssignThema().setAssignUser(null);
				themaRepository.flush();
			}
			userRepository.delete(user.get());
			userRepository.flush();
			return true;
		}
		return false;
	}

	/**
	 * This method adds the given user object to the repository if the his nickname
	 * is not present and return the status of the operation.
	 * 
	 * @param user, user to add
	 * @return true/false, save operation was successful or not
	 */
	@Override
	public boolean saveUser(User user) {
		if (!userRepository.findByNickname(user.getNickname()).isPresent()) {
			user.setPasswordHash(new BCryptPasswordEncoder(10).encode(user.getPasswordHash()));
			User savedUser = userRepository.save(user);
			if (savedUser != null)
				return true;
		}
		return false;
	}

	/**
	 * This method returns the priority map of the given user object.
	 * 
	 * @param user, user to get his map of priority's
	 * @return Map<Thema,Priority>, User map of priority for the thema's
	 */
	@Override
	public Map<Thema, Priority> getUserPriority(User user) {
		return user.getAbgebenePrios();
	}

	/**
	 * This method changes the status of the privacy message accept or not of the
	 * given user object.
	 * 
	 * @param status,         new status of his privacy message status
	 * @param Optional<User>, user to set his new status
	 */
	@Override
	public void changeDataProtectionStatus(Boolean status, Optional<User> user) {
		if (user.isPresent()) {
			user.get().setDataProtectionAccepted(status);
			userRepository.save(user.get());
		}
	}

	/**
	 * This method adds the given list of prioritys to the given user object.
	 * 
	 * @param prios,          list of thema and priority's
	 * @param Optional<User>, user to add his new priority
	 * 
	 * @return true/false, save Operation of the prios was successful or not
	 */
	@Override
	public boolean savePriosForUser(List<UserPriosDataDTO> prios, Optional<User> user) {
		if (user.isPresent()) {
			for (int i = 0; i < prios.size(); i++) {
				user.get().addAbgegebenePrio(themaService.getThema(prios.get(i).getThemaId()).get(),
						Priority.valueOf(prios.get(i).getPrio()));
			}
			userRepository.flush();
			return true;
		}
		return false;
	}

	/**
	 * This method returns a optional of a user in the repository.
	 * 
	 * @param name, nickname of the user to get his object
	 * @return Optional<User>, Optional of user in repository
	 */
	@Override
	public Optional<User> getUserByName(String nickname) {
		return userRepository.findByNickname(nickname);
	}

	/**
	 * This method returns a optional of a user in the repository.
	 * 
	 * @param name, nickname of the user to get his object
	 * @return Optional<User>, Optional of user in repository
	 */
	@Override
	public Optional<User> getUserByID(Long userID) {
		return userRepository.findById(userID);
	}
}
