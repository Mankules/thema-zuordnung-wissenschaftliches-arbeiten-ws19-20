package com.fhzwickau.wissarbeit.user.boundary;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fhzwickau.wissarbeit.config.ApiError;
import com.fhzwickau.wissarbeit.config.GeneralErrorException;
import com.fhzwickau.wissarbeit.dto.CreateUserDTO;
import com.fhzwickau.wissarbeit.security.service.CurrentUserUtil;
import com.fhzwickau.wissarbeit.security.service.DataProtectionMessageService;
import com.fhzwickau.wissarbeit.user.domain.Role;
import com.fhzwickau.wissarbeit.user.domain.User;
import com.fhzwickau.wissarbeit.user.service.UserService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UserController {

	Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private DataProtectionMessageService dpmService;

	/**
	 * Checks if the current User is a Admin
	 * 
	 * @return true/false is User Role Admin
	 */
	private boolean isUserAdmin() {
		Optional<User> user = CurrentUserUtil.getCurrentUser(userService);
		if (user.isPresent()) {
			if (user.get().getRole().equals(Role.ADMIN)) {
				return true;
			}
			return false;
		}
		return false;
	}

	/**
	 * This method returns a JSON with a DTO of all User and Thema Objects to show
	 * the data.
	 * 
	 * @return AdminDataDTO
	 */
	@GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllUsers() throws GeneralErrorException {
		if (isUserAdmin()) {
			return ResponseEntity.status(HttpStatus.OK).body(userService.getAdminDataDTO());
		}
		throw new GeneralErrorException(new ApiError(HttpStatus.BAD_REQUEST,
				"Der User hat keine Rechte zum anzeigen von anderen Usern und Themas"));
	}

	/**
	 * This method takes the given JSON and simple checks if the data is not null.
	 * After this he calls the UserService to register the new User.
	 * 
	 * @return Status
	 */
	@PostMapping(value = "/register", consumes = "application/json")
	public ResponseEntity<?> registerUser(@RequestBody CreateUserDTO user) throws GeneralErrorException {
		if (user == null || user.getUsername().isEmpty() || user.getName().isEmpty() || user.getSurname().isEmpty()
				|| user.getPasswordHash() == null || user.getPasswordHash().isEmpty()
				|| user.getPasswordHash().equalsIgnoreCase("null")) {
			throw new GeneralErrorException(
					new ApiError(HttpStatus.BAD_REQUEST, "Der Registrierungsdaten sind ungültig"));
		}
		if (userService.getUserByName(user.getUsername()).isPresent()) {
			logger.info("Der Benutzername " + user.getUsername() + " ist bereits vergeben.");
			throw new GeneralErrorException(
					new ApiError(HttpStatus.BAD_REQUEST, "Der Benutzername ist bereits vergeben"));
		}
		if (userService.saveUser(new User(user.getName(), user.getSurname(), Role.STUDENT, user.getUsername(),
				user.getPasswordHash()))) {
			logger.info("Der Benutzer " + user.getUsername() + " wurde erfolgreich registriert.");
			return ResponseEntity.status(HttpStatus.OK).body("Die Registrierung wurde erfolgreich abgeschlossen");
		}
		throw new GeneralErrorException(new ApiError(HttpStatus.BAD_REQUEST, "Die Registrierung ist nicht möglich"));
	}

	/**
	 * This method calls the UserService to delete the User from the given Path.
	 * 
	 * @param userId, User to delete
	 * 
	 * @return Status
	 */
	@DeleteMapping(value = "/users/{userId}/delete")
	public ResponseEntity<?> deleteUser(@PathVariable String userId) throws GeneralErrorException {
		if (isUserAdmin()) {
			if (userService.removeUser(userId)) {
				logger.info("Der Admin hat einen User gelöscht.");
				return ResponseEntity.status(HttpStatus.OK).body("Der User wurde erfolgreich gelöscht");
			}
			throw new GeneralErrorException(new ApiError(HttpStatus.BAD_REQUEST,
					"Der User konnte nicht gelöscht werden da er nicht gefunden wurde"));
		}
		throw new GeneralErrorException(
				new ApiError(HttpStatus.BAD_REQUEST, "Der User hat keine Rechte zum löschen von anderen User"));
	}

	/**
	 * This method calls the DataProtectionService to get the privacy message.
	 * 
	 * @return Status
	 */
	@GetMapping(value = "/privacy")
	public ResponseEntity<?> getPrivacyPolicy() {
		return ResponseEntity.status(HttpStatus.OK).body(dpmService.getDataProtectionMessage());
	}

	/**
	 * This method calls the DataProtectionService to set the new privacy message.
	 * 
	 * @param newJson, new changed privacy message
	 * 
	 * @return Status
	 */
	@PostMapping(value = "/privacy/edit")
	public ResponseEntity<?> editPrivacyPolicy(@RequestBody String newJSON) throws Exception {
		System.out.println("privacy edit aufgerufen");
		System.out.println(newJSON);
		ObjectNode json = null;
		String text = "";
		try {
			json = new ObjectMapper().readValue(newJSON, ObjectNode.class);
			text = json.get("text").asText();
		} catch (Exception e) {
			throw new GeneralErrorException(
					new ApiError(HttpStatus.BAD_REQUEST, "Der Request konnte vom Server nicht gelesen werden."));
		}
		User u = CurrentUserUtil.getCurrentUser(userService).get();
		logger.info("Die Datenschutzerklärung wurde von  " + u.getNickname() + " verändert.");
		if (dpmService.setDataProtectionMessage(text)) {
			return ResponseEntity.status(HttpStatus.OK).body(dpmService.getDataProtectionMessage());
		}
		throw new GeneralErrorException(
				new ApiError(HttpStatus.BAD_REQUEST, "Die Datenschutzerklärung konnte nicht verändert werden."));
	}

	/**
	 * This method calls the UserService to change the privacy accepted status of
	 * the current user.
	 */
	@PatchMapping(value = "/users/protection")
	@ResponseStatus(HttpStatus.OK)
	public void changeDataProtectionStatus() {
		Optional<User> user = CurrentUserUtil.getCurrentUser(userService);
		if (user.isPresent()) {
			userService.changeDataProtectionStatus(true, user);

		}
	}

	/**
	 * This method calls the UserService to get the User object from the
	 * authentication.
	 * 
	 * @return User
	 */
	@GetMapping("/users/get")
	public User getUser() {
		return userService.getUserByName(SecurityContextHolder.getContext().getAuthentication().getName()).get();
	}
}
