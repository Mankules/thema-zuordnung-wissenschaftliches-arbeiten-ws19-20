package com.fhzwickau.wissarbeit.user.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.fhzwickau.wissarbeit.dto.AdminDataDTO;
import com.fhzwickau.wissarbeit.dto.UserPriosDataDTO;
import com.fhzwickau.wissarbeit.thema.domain.Priority;
import com.fhzwickau.wissarbeit.thema.domain.Thema;
import com.fhzwickau.wissarbeit.user.domain.User;

public interface UserService {

	public AdminDataDTO getAdminDataDTO();

	public void addUser(User user);

	public boolean removeUser(String userId);

	public boolean saveUser(User user);

	public Optional<User> getUserByName(String name);

	public void changeDataProtectionStatus(Boolean status, Optional<User> user);

	public boolean savePriosForUser(List<UserPriosDataDTO> prios, Optional<User> user);

	public Map<Thema, Priority> getUserPriority(User user);

	public Optional<User> getUserByID(Long userID);

}
