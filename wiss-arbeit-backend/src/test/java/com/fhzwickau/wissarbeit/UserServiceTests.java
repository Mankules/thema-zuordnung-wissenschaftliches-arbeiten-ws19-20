package com.fhzwickau.wissarbeit;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fhzwickau.wissarbeit.dto.UserPriosDataDTO;
import com.fhzwickau.wissarbeit.thema.domain.Priority;
import com.fhzwickau.wissarbeit.thema.domain.Thema;
import com.fhzwickau.wissarbeit.thema.domain.ThemaRepository;
import com.fhzwickau.wissarbeit.thema.service.ThemaService;
import com.fhzwickau.wissarbeit.user.domain.Role;
import com.fhzwickau.wissarbeit.user.domain.User;
import com.fhzwickau.wissarbeit.user.domain.UserRepository;
import com.fhzwickau.wissarbeit.user.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WissArbeitBackendApplication.class)
public class UserServiceTests {

	@Autowired
	UserRepository userRepository;
	@Autowired
	ThemaRepository themaRepository;
	@Autowired
	ThemaService themaService;
	@Autowired
	UserService userService;

	User user1 = new User("name1", "nachname1", Role.STUDENT, "nickname1", "pw1");
	User user2 = new User("name2", "nachname2", Role.STUDENT, "nickname2", "pw2");
	Thema thema1 = new Thema("themaname1", "beschreibung1");
	Thema thema2 = new Thema("themaname2", "beschreibung2");

	@Before
	public void generateTestData() {
		userRepository.deleteAll();
		themaRepository.deleteAll();

		userRepository.save(user1);
		userRepository.save(user2);
		themaRepository.save(thema1);
		themaRepository.save(thema2);
	}

	@Test
	@Transactional
	public void testGetAdminDataUserDTOs() {
		assertEquals(2,userService.getAdminDataDTO().getUsers().size());
	}
	
	@Test
	@Transactional
	public void testRemoveUser() {
		assertTrue(userService.removeUser(user1.getId().toString()));
	}

	@Test
	@Transactional
	public void testSaveUser() {
		assertTrue(userService.saveUser(new User("testname", "testnachname", Role.STUDENT, "testnickname", "testpw")));
		assertFalse(userService.saveUser(new User("testname", "testnachname", Role.STUDENT, "testnickname", "testpw")));
		assertEquals(3, userRepository.findAll().size());
	}

	@Test
	@Transactional
	public void testGetUserByName() {
		assertEquals(user1, userService.getUserByName(user1.getNickname()).get());
	}

	@Test
	@Transactional
	public void testGetUserByID() {
		assertEquals(user1, userService.getUserByID(user1.getId()).get());
	}

	@Test
	@Transactional
	public void testSavePrios() {
		List<UserPriosDataDTO> prios = new ArrayList<UserPriosDataDTO>();
		UserPriosDataDTO prio1 = new UserPriosDataDTO(thema1.getId().toString(), Priority.FAVORIT.toString());
		UserPriosDataDTO prio2 = new UserPriosDataDTO(thema2.getId().toString(), Priority.GUT.toString());
		prios.add(prio1);
		prios.add(prio2);

		assertTrue(userService.savePriosForUser(prios, Optional.of(user1)));
		assertEquals(2, user1.getAbgebenePrios().size());
	}

}
