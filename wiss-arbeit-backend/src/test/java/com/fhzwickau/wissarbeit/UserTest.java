package com.fhzwickau.wissarbeit;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.fhzwickau.wissarbeit.thema.domain.Priority;
import com.fhzwickau.wissarbeit.thema.domain.Thema;
import com.fhzwickau.wissarbeit.user.domain.Role;
import com.fhzwickau.wissarbeit.user.domain.User;

public class UserTest {

	User user = new User("name","nachname",Role.STUDENT,"nickname","pw");
	Thema thema = new Thema("themaname","beschreibung");
	Thema thema1 = new Thema("themaname1","beschreibung1");
	
	@Before
	public void generateTestData(){
		user.addAbgegebenePrio(thema, Priority.FAVORIT);
		user.addAbgegebenePrio(thema1, Priority.GUT);
		user.addExcludeThema(thema);
	}
	
	@Test
	public void testAddAbgegebenePrio() {	
		user.addAbgegebenePrio(thema, Priority.SCHLECHT);
		
		assertEquals(2,user.getAbgebenePrios().size());
	}

	@Test
	public void testContainsAbgebenePrio() {
		assertTrue(user.containsAbgebenePrio(thema));
	}

	@Test
	public void testRemoveThemaInAbgebenePrios() {
		user.removeThemaInAbgebenePrios(thema);
		
		assertEquals(1,user.getAbgebenePrios().size());
	}

	@Test
	public void testGetPriorityForThema() {
		assertEquals(Priority.FAVORIT, user.getPriorityForThema(thema));
	}

	@Test
	public void testAddExcludeThema() {
		assertEquals(1,user.getExcludeThemas().size());
	}

	@Test
	public void testRemoveExcludeThema() {
		assertTrue(user.removeExcludeThema(thema));
	}

	@Test
	public void testContainsExcludeThema() {
		assertTrue(user.containsExcludeThema(thema));
	}
}
