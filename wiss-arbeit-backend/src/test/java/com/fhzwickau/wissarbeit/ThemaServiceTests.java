package com.fhzwickau.wissarbeit;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fhzwickau.wissarbeit.dto.CreateThemaDTO;
import com.fhzwickau.wissarbeit.dto.ThemaDataDTO;
import com.fhzwickau.wissarbeit.dto.UserPriosDTO;
import com.fhzwickau.wissarbeit.dto.UserPriosDataDTO;
import com.fhzwickau.wissarbeit.thema.domain.Priority;
import com.fhzwickau.wissarbeit.thema.domain.Thema;
import com.fhzwickau.wissarbeit.thema.domain.ThemaRepository;
import com.fhzwickau.wissarbeit.thema.service.ThemaService;
import com.fhzwickau.wissarbeit.user.domain.Role;
import com.fhzwickau.wissarbeit.user.domain.User;
import com.fhzwickau.wissarbeit.user.domain.UserRepository;
import com.fhzwickau.wissarbeit.user.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WissArbeitBackendApplication.class)
public class ThemaServiceTests {
	
	@Autowired
	UserRepository userRepository;
	@Autowired
	ThemaRepository themaRepository;
	@Autowired
	ThemaService themaService;
	@Autowired
	UserService userService;

	User user1 = new User("name1", "nachname1", Role.STUDENT, "nickname1", "pw1");
	User user2 = new User("name2", "nachname2", Role.STUDENT, "nickname2", "pw2");
	Thema thema1 = new Thema("themaname1", "beschreibung1");
	Thema thema2 = new Thema("themaname2", "beschreibung2");
	
	@Before
	public void generateTestData() {
		userRepository.deleteAll();
		themaRepository.deleteAll();

		userRepository.save(user1);
		userRepository.save(user2);
		themaRepository.save(thema1);
		themaRepository.save(thema2);
	}
	
	@Test
	@Transactional
	public void testGetThema() {
		assertEquals(thema1,themaService.getThema(thema1.getId().toString()).get());
	}
	
	@Test
	@Transactional
	public void testGetAllThemas() {
		assertEquals(2, themaService.getAllThemas(Optional.of(user1)).getThemas().size());
	}

	@Test
	@Transactional
	public void testSavePrios() {
		List<UserPriosDataDTO> prios = new ArrayList<UserPriosDataDTO>();
		UserPriosDataDTO data1 = new UserPriosDataDTO(thema1.getId().toString(),Priority.FAVORIT.toString());
		UserPriosDataDTO data2 = new UserPriosDataDTO(thema2.getId().toString(),Priority.GUT.toString());
		prios.add(data1);
		prios.add(data2);
		
		assertTrue(themaService.savePrios(new UserPriosDTO(prios), Optional.of(user1)));
	}
	
	@Test
	@Transactional
	public void testGetAdminDataThemaDTOs() {
		assertEquals(2,themaService.getAdminDataThemaDTOs().size());
	}
	
	@Test
	@Transactional
	public void testGetAllThemaDataForUser() {	
		assertEquals(2,themaService.getAllThemaDataForUser(user1.getId().toString()).getAllowThemas().size());
		assertEquals(0,themaService.getAllThemaDataForUser(user1.getId().toString()).getExcludeThemas().size());
	}
	
	@Test
	@Transactional
	public void testAssignThema() {
		assertTrue(themaService.assignThema(thema1.getId().toString(), user1.getId().toString()));
	}
	
	@Test
	@Transactional
	public void testDeleteAssignThema() {
		themaService.assignThema(thema1.getId().toString(), user1.getId().toString());
		assertTrue(themaService.deleteAssignThema(thema1.getId().toString(), user1.getId().toString()));
	}
	
	@Test
	@Transactional
	public void testAllowThema() {
		themaService.excludeThema(thema1.getId().toString(), user1.getId().toString());
		assertTrue(themaService.allowThema(thema1.getId().toString(), user1.getId().toString()));
	}
	
	@Test
	@Transactional
	public void testExcludeThema() {
		assertTrue(themaService.excludeThema(thema1.getId().toString(), user1.getId().toString()));
	}
	
	@Test
	@Transactional
	public void testCreateThema() {
		assertTrue(themaService.createThema(new CreateThemaDTO("neuer Name","neue Beschreibung")));
	}
	
	@Test
	@Transactional
	public void testEditThema() {
		assertTrue(themaService.editThema(thema1.getId().toString(), new ThemaDataDTO("neuer name","neue Beschreibung")));
	}
	
	@Test
	@Transactional
	public void testGetThemaDataDTO() {
		ThemaDataDTO dto = new ThemaDataDTO(thema1.getName(),thema1.getDescription());
		assertEquals(dto.getName(), themaService.getThemaDataDTO(thema1.getId().toString()).getName());
		assertEquals(dto.getDescription(), themaService.getThemaDataDTO(thema1.getId().toString()).getDescription());

	}
	
	@Test
	@Transactional
	public void testDeleteThema() {
		assertTrue(themaService.deleteThema(thema1.getId().toString()));
		assertFalse(themaService.deleteThema(thema1.getId().toString()));
	}
}
